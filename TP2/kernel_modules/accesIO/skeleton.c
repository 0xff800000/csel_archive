#include <linux/module.h> // modules
#include <linux/init.h> // macros
#include <linux/kernel.h> // debugging
#include <linux/string.h> // handles strings
#include <linux/list.h> // listes
#include <linux/slab.h>
#include <linux/gfp.h>
#include <linux/moduleparam.h> // kernel parameters
#include <linux/ioport.h> // IO
#include <linux/io.h> // IO

static const unsigned long CHIP_ID_ADDR = 0x01c14200;
static const unsigned long TEMP_ADDR = 0x01c25080;
static const unsigned long MAC_ADDR = 0x01c30050;
int*chip_id;
int*temp_sens;
int*mac_addr;
struct resource* chipID_req;

// Module init function
static int __init skeleton_init(void){
  // Variables
  int reg, temp;

  pr_info ("** Linux IO module loaded **\n");

  // Request memory regions
  chipID_req = request_mem_region(CHIP_ID_ADDR,  0xd, "acessIO module : ChipID");

  if(chipID_req == NULL){
    pr_info("ERROR : Failed to request Chip ID ressource\n");
    return 0;
  }

  // Map memory regions
  chip_id = (int*) ioremap(CHIP_ID_ADDR, 4096);
  temp_sens = (int*) ioremap(TEMP_ADDR, 4096);
  mac_addr = (int*) ioremap(MAC_ADDR, 4096);

  if(chip_id == NULL){
    pr_info("ERROR : Failed to map Chip ID\n");
    return 0;
  }

  if(temp_sens == NULL){
    pr_info("ERROR : Failed to map Temperature sensor\n");
    return 0;
  }

  if(mac_addr == NULL){
    pr_info("ERROR : Failed to map MAC address\n");
    return 0;
  }

  // Read registers
  pr_info("Chip ID : 0x%.8x%.8x%.8x%.8x\n",
    ioread32(chip_id+0),
    ioread32(chip_id+1),
    ioread32(chip_id+2),
    ioread32(chip_id+3)
  );

  reg = ioread32(temp_sens);
  temp = (int)(217000-(reg*1000000)/8253);
  pr_info("Temperature : %d mCelsius (0x%.8x)\n",
    temp,
    reg
  );

  pr_info("MAC address : 0x%.8x%.8x\n",
    ioread32(mac_addr+0),
    ioread32(mac_addr+1)
  );

  return 0;
}

// Module exit function
static void __exit skeleton_exit(void){
  pr_info ("** Linux module memory unloading **\n");

  if(chip_id != NULL){
    iounmap((void*)CHIP_ID_ADDR);
  }

  if(temp_sens != NULL){
    iounmap((void*)TEMP_ADDR);
  }

  if(mac_addr != NULL){
    iounmap((void*)MAC_ADDR);
  }

  if(chipID_req != NULL) release_mem_region(CHIP_ID_ADDR, 0xd);

  pr_info ("** Linux module memory unloading done **\n");
}

module_init (skeleton_init);
module_exit (skeleton_exit);

MODULE_AUTHOR ("Dan Yvan Baumgartner <dan.baumgartner@master.hes-so.ch>");
MODULE_DESCRIPTION ("Module skeleton");
MODULE_LICENSE ("GPL");


