#include <linux/module.h> // modules
#include <linux/init.h> // macros
#include <linux/kernel.h> // debugging
#include <linux/string.h> // handles strings
#include <linux/list.h> // listes
#include <linux/slab.h>
#include <linux/gfp.h>
#include <linux/moduleparam.h> // kernel parameters
#include <linux/io.h> // entrées/sorties


// Kernel parameters
static char* text= "default_str";
module_param(text, charp, 0);
static int elements=1;
module_param(elements, int, 0);

struct list_element{
  int id;
  char*str;
  struct list_head node; // Kernel list structure
};

// struct list_element mylinked_list;
static LIST_HEAD (mylinked_list);

// Module init function
static int __init skeleton_init(void){
  // Variables
  int i;
  struct list_element* ele;
  char*ele_str;
  struct list_head * node;
  
  pr_info ("** Linux memory module loaded **\n");
  pr_info ("Creating %d elements containing \"%s\"\n", elements, text);

  // Memory allocation
  for(i=0; i<elements; i++){
    ele = kzalloc(sizeof(struct list_element), GFP_KERNEL);
    if(ele != NULL){
      ele->id = i;
      ele_str = (char*) kzalloc(strlen(text), GFP_KERNEL);
      if(ele_str != NULL){
        strcpy(ele_str, text);
        ele->str = ele_str;
      }
      list_add(&ele->node, &mylinked_list);
    }
  }

  // Parse list
  list_for_each(node, &mylinked_list){
    ele = list_entry(node, struct list_element, node);
    if(ele != NULL){
      pr_info("Element id:%d allocated with text:%s\n", ele->id, ele->str);
    }
  }

  return 0;
}

// Module exit function
static void __exit skeleton_exit(void){
  struct list_element * ele;
  struct list_element * tmp;
  pr_info ("** Linux module memory unloading **\n");
  // Free list
  list_for_each_entry_safe(ele, tmp, &mylinked_list, node){
    list_del(&ele->node);
    kfree(ele->str);
    kfree(ele);
  }
  pr_info ("** Linux module memory unloading done **\n");
}

module_init (skeleton_init);
module_exit (skeleton_exit);

MODULE_AUTHOR ("Dan Yvan Baumgartner <dan.baumgartner@master.hes-so.ch>");
MODULE_DESCRIPTION ("Module skeleton");
MODULE_LICENSE ("GPL");


