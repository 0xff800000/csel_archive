#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/gfp.h>
#include <linux/delay.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>

// Pins descriptions
static const unsigned int K1_io = 0;
static const unsigned int K2_io = 2;
static const unsigned int K3_io = 3;

static unsigned int resIRQk1,resIRQk2,resIRQk3;
static unsigned int irqNumber_k1;
static unsigned int irqNumber_k2;
static unsigned int irqNumber_k3;

// IRQ handlers
static irq_handler_t gpio_irq_handler_k1(unsigned int irq, void*dev_id, struct pt_regs * regs){
  pr_info("Button k1 pressed\n");
  return (irq_handler_t) IRQ_HANDLED;
}

static irq_handler_t gpio_irq_handler_k2(unsigned int irq, void*dev_id, struct pt_regs * regs){
  pr_info("Button k2 pressed\n");
  return (irq_handler_t) IRQ_HANDLED;
}

static irq_handler_t gpio_irq_handler_k3(unsigned int irq, void*dev_id, struct pt_regs * regs){
  pr_info("Button k3 pressed\n");
  return (irq_handler_t) IRQ_HANDLED;
}



// Module init function
static int __init skeleton_init(void){
  pr_info ("** Linux GPIO IRQ module loaded **\n");

  // Request pins
  gpio_request(K1_io, "sysfs");
  gpio_request(K2_io, "sysfs");
  gpio_request(K3_io, "sysfs");

  irqNumber_k1 = gpio_to_irq(K1_io);
  irqNumber_k2 = gpio_to_irq(K2_io);
  irqNumber_k3 = gpio_to_irq(K3_io);
  if(irqNumber_k1 == 0 || irqNumber_k2 == 0 || irqNumber_k3 == 0){
    pr_info("ERROR :could not get GPIO IRQ number\n");
    return 0;
  }
  else{
    pr_info("GPIO IRQ number : %d, %d, %d\n", irqNumber_k1, irqNumber_k2, irqNumber_k3);
  }

  resIRQk1 = request_irq(
    irqNumber_k1,
    (irq_handler_t) gpio_irq_handler_k1,
    IRQF_TRIGGER_RISING,
    "mymoduleGPIOirqK1",
    NULL
  );
  
  resIRQk2 = request_irq(
    irqNumber_k2,
    (irq_handler_t) gpio_irq_handler_k2,
    IRQF_TRIGGER_RISING,
    "mymoduleGPIOirqK2",
    NULL
  );

  resIRQk3 = request_irq(
    irqNumber_k3,
    (irq_handler_t) gpio_irq_handler_k3,
    IRQF_TRIGGER_RISING,
    "mymoduleGPIOirqK3",
    NULL
  );

  if(resIRQk1 != 0 || resIRQk2 != 0 || resIRQk3 != 0){
    pr_info("ERROR : One of IRQs already in use\n");
    return 0;
  }
  

  return 0;
}

// Module exit function
static void __exit skeleton_exit(void){
  pr_info ("** Linux GPIO IRQ module unloading **\n");

  // Free pins
  gpio_free(K1_io);
  gpio_free(K2_io);
  gpio_free(K3_io);

  // Free GPIO IRQ
  if(resIRQk1 == 0) free_irq(irqNumber_k1, NULL);
  if(resIRQk2 == 0) free_irq(irqNumber_k2, NULL);
  if(resIRQk3 == 0) free_irq(irqNumber_k3, NULL);

  pr_info ("** Linux GPIO IRQ module unloading done **\n");
}

module_init (skeleton_init);
module_exit (skeleton_exit);

MODULE_AUTHOR ("Dan Yvan Baumgartner <dan.baumgartner@master.hes-so.ch>");
MODULE_DESCRIPTION ("Module skeleton");
MODULE_LICENSE ("GPL");


