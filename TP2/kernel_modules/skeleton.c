/* skeleton.c */
#include <linux/module.h> /* modules */
#include <linux/init.h> /* macros */
#include <linux/kernel.h> /* debugging */
#include <linux/string.h> /* handles strings */
#include <linux/list.h> /* listes */
#include <linux/slab.h>
#include <linux/gfp.h>
#include <linux/moduleparam.h> /* kernel parameters */
#include <linux/io.h> /* entrées/sorties */


/* Kernel parameters*/
static char* text= "baffe";
module_param(text, charp, 0);
static int elements=1;
module_param(elements, int, 0);

/* Module init function */
static int __init skeleton_init(void)
{
  pr_info ("** Linux module skeleton loaded **\n");
  pr_info ("text: %s\nelements: %d\n", text, elements);

  
  /* Dynamic allocation*/
  char* buffer;
  buffer = (char*)kzalloc(sizeof(char)*100, GFP_KERNEL);
  if (buffer == NULL)
    { /* manage error */
      pr_info("// Error with dynamic memory allocation \\");
    }
  else
    {
      pr_info("I allocated %zu bytes of memory\n", ksize(buffer)); 
    }
  sprintf(buffer, "Text in dynamic memory\n");
  pr_info("%s", buffer);

  /* Chip ID*/
  unsigned int chipid[4]={0};
  void *addr = (void*)0x01c14200;
  int i;
  /*
  for(i=0; i<4; i++)
    {
      chipid[i] = ioread32(addr);
      pr_info("=== Chip-ID %d: %n\n", i, chipid);
      addr += 3;
    }
  */
  
  /* Free dynamicaly allocated memory */
  kfree(buffer); 
  return 0;
}

/* Module exit function */
static void __exit skeleton_exit(void)
{
   pr_info ("** Linux module skeleton unloaded **\n");
}

module_init (skeleton_init);
module_exit (skeleton_exit);

MODULE_AUTHOR ("Vincent Conus <vincent.conus@master.hes-so.ch>");
MODULE_DESCRIPTION ("Module skeleton");
MODULE_LICENSE ("GPL");


