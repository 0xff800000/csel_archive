#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/gfp.h>
#include <linux/kthread.h>
#include <linux/delay.h>
#include <linux/wait.h>

struct task_struct* task1;
struct task_struct* task2;
int wait_condition;

DECLARE_WAIT_QUEUE_HEAD(queue);

// Threads
int thread1(void*data){
  while(!kthread_should_stop()){
    wait_event_killable(queue, wait_condition == 1);
    pr_info("Thread 1 : woke up\n");
    wait_condition = 0;
  }
  return 0;
}

int thread2(void*data){
  while(!kthread_should_stop()){
    pr_info("Thread 2 : waking thread 1 up\n");
    wait_condition = 1;
    wake_up(&queue);
    pr_info("Thread 2 : falling asleep for 5 sec\n");
    ssleep(5);
  }
  return 0;
}


// Module init function
static int __init skeleton_init(void){
  pr_info ("** Linux sleep module loaded **\n");

  task1 = kthread_run(thread1, NULL,"thread 1");
  task2 = kthread_run(thread2, NULL,"thread 2");

  return 0;
}

// Module exit function
static void __exit skeleton_exit(void){
  pr_info ("** Linux module sleep unloading **\n");

  wait_condition = 1;
  wake_up(&queue);

  if(task1 != NULL) kthread_stop(task1);
  if(task2 != NULL) kthread_stop(task2);

  pr_info ("** Linux module sleep unloading done **\n");
}

module_init (skeleton_init);
module_exit (skeleton_exit);

MODULE_AUTHOR ("Dan Yvan Baumgartner <dan.baumgartner@master.hes-so.ch>");
MODULE_DESCRIPTION ("Module skeleton");
MODULE_LICENSE ("GPL");


