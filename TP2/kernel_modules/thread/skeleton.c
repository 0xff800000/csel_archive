#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/gfp.h>
#include <linux/kthread.h>
#include <linux/delay.h>

struct task_struct* message_task;

// Thread
int thread(void*data){
  while(!kthread_should_stop()){
    pr_info("Periodic message\n");
    ssleep(5);
  }
  return 0;
}

// Module init function
static int __init skeleton_init(void){
  pr_info ("** Linux thread module loaded **\n");

  message_task = kthread_run(thread, NULL,"Periodic thread");

  return 0;
}

// Module exit function
static void __exit skeleton_exit(void){
  pr_info ("** Linux thread module unloading **\n");

  if(message_task != NULL) kthread_stop(message_task);

  pr_info ("** Linux thread module unloading done **\n");
}

module_init (skeleton_init);
module_exit (skeleton_exit);

MODULE_AUTHOR ("Dan Yvan Baumgartner <dan.baumgartner@master.hes-so.ch>");
MODULE_DESCRIPTION ("Module skeleton");
MODULE_LICENSE ("GPL");


