﻿#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>

#define NB_PINS 3

int main(){
    fd_set rfds,master;
    int maxfd=-1;
    int Kn[NB_PINS] = {0};
    int fds[NB_PINS] = {0};

    FD_ZERO(&rfds);
    FD_ZERO(&master);

    for(int i=0; i<NB_PINS; i++){
        char path[128];
        sprintf(path, "/dev/myDevice%i", i);
        fds[i] = open(path, O_RDONLY);
        if(fds[i] < 0) {
            perror("Open /dev/myDevice");
            return -1;
        }
        FD_SET(fds[i],&master);
        if(maxfd < fds[i]) maxfd = fds[i];
        printf("%s opened with fd=%i\n",path,fds[i]);
    }
    
    while(1){
        memcpy(&rfds, &master, sizeof(master));
        int retval = select(maxfd+1, &rfds, NULL, NULL, NULL);
        if(retval == -1){
            perror("select()");
            return -1;
        }
        else if(retval){
            printf("Pin interrupt : ");
            for(int i=0; i<NB_PINS; i++){
                if(FD_ISSET(fds[i], &rfds)){
                    Kn[i]++;
                    char nil;
                    read(fds[i],&nil,1);
                }
                printf("K%i=%i ",i,Kn[i]);
            }
            printf("\n");
        }
    }

    return 0;
}