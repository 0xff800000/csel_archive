﻿#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/gfp.h>
#include <linux/delay.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>
#include <linux/sched.h>
#include <linux/wait.h>
#include <linux/miscdevice.h>
#include <linux/poll.h>

// Pins descriptions
static const unsigned int K1_io = 0;
static const unsigned int K2_io = 2;
static const unsigned int K3_io = 3;

static unsigned int resIRQk1,resIRQk2,resIRQk3;
static unsigned int irqNumber_k1;
static unsigned int irqNumber_k2;
static unsigned int irqNumber_k3;

// Skeleton prototypes
static unsigned int skeleton_poll_k1(struct file *f, poll_table *wait);
static unsigned int skeleton_poll_k2(struct file *f, poll_table *wait);
static unsigned int skeleton_poll_k3(struct file *f, poll_table *wait);
static ssize_t skeleton_read_k1(struct file *f, char __user *buf, size_t count, loff_t *off);
static ssize_t skeleton_read_k2(struct file *f, char __user *buf, size_t count, loff_t *off);
static ssize_t skeleton_read_k3(struct file *f, char __user *buf, size_t count, loff_t *off);
static int skeleton_open(struct inode *i, struct file *f);
static int skeleton_release (struct inode *i, struct file *f);


struct list_element{
  int id;
  int request_ready;
  struct file_operations skeleton_fops;
  wait_queue_head_t queue;
};

#define instances 3
static struct list_element mylist[instances] = {
  [0]= {
    .request_ready = 0,
    .skeleton_fops = {
      .owner = THIS_MODULE,
      .open = skeleton_open,
      .read = skeleton_read_k1,
      .release = skeleton_release,
      .poll = skeleton_poll_k1,
    },
  },
  [1]= {
    .request_ready = 0,
    .skeleton_fops = {
      .owner = THIS_MODULE,
      .open = skeleton_open,
      .read = skeleton_read_k2,
      .release = skeleton_release,
      .poll = skeleton_poll_k2,
    },
  },
  [2]= {
    .request_ready = 0,
    .skeleton_fops = {
      .owner = THIS_MODULE,
      .open = skeleton_open,
      .read = skeleton_read_k3,
      .release = skeleton_release,
      .poll = skeleton_poll_k3,
    },
  },
};

static struct miscdevice * mydevice_miscdev_array;

// IRQ handlers
static irq_handler_t gpio_irq_handler_k1(unsigned int irq, void*dev_id, struct pt_regs * regs){
  pr_info("skeleton : Button k1 pressed\n");
  mylist[0].request_ready = 1;
  wake_up_interruptible(&mylist[0].queue);
  return (irq_handler_t) IRQ_HANDLED;
}

static irq_handler_t gpio_irq_handler_k2(unsigned int irq, void*dev_id, struct pt_regs * regs){
  pr_info("skeleton : Button k2 pressed\n");
  mylist[1].request_ready = 1;
  wake_up_interruptible(&mylist[1].queue);
  return (irq_handler_t) IRQ_HANDLED;
}

static irq_handler_t gpio_irq_handler_k3(unsigned int irq, void*dev_id, struct pt_regs * regs){
  pr_info("skeleton : Button k3 pressed\n");
  mylist[2].request_ready = 1;
  wake_up_interruptible(&mylist[2].queue);
  return (irq_handler_t) IRQ_HANDLED;
}


static unsigned int skeleton_poll_k1(struct file *f, poll_table *wait){
  unsigned mask = 0;
  poll_wait(f, &mylist[0].queue, wait);
  if(mylist[0].request_ready != 0) {
    mask |= POLLIN | POLLRDNORM;
  }
  return mask;
}

static unsigned int skeleton_poll_k2(struct file *f, poll_table *wait){
  unsigned mask = 0;
  poll_wait(f, &mylist[1].queue, wait);
  if(mylist[1].request_ready != 0) {
    mask |= POLLIN | POLLRDNORM;
  }
  return mask;
}

static unsigned int skeleton_poll_k3(struct file *f, poll_table *wait){
  unsigned mask = 0;
  poll_wait(f, &mylist[2].queue, wait);
  if(mylist[2].request_ready != 0) {
    mask |= POLLIN | POLLRDNORM;
  }
  return mask;
}


static ssize_t skeleton_read_k1(struct file *f, char __user *buf, size_t count, loff_t *off){
  char one = '1';
  wait_event_interruptible(mylist[0].queue, mylist[0].request_ready != 0);

  count = 1;
  if(copy_to_user (buf, &one, count) != 0) count = -EFAULT;
  mylist[0].request_ready = 0;

  return count;
}

static ssize_t skeleton_read_k2(struct file *f, char __user *buf, size_t count, loff_t *off){
  char one = '1';
  wait_event_interruptible(mylist[1].queue, mylist[1].request_ready != 0);

  count = 1;
  if(copy_to_user (buf, &one, count) != 0) count = -EFAULT;
  mylist[1].request_ready = 0;

  return count;
}

static ssize_t skeleton_read_k3(struct file *f, char __user *buf, size_t count, loff_t *off){
  char one = '1';
  wait_event_interruptible(mylist[2].queue, mylist[2].request_ready != 0);

  count = 1;
  if(copy_to_user (buf, &one, count) != 0) count = -EFAULT;
  mylist[2].request_ready = 0;

  return count;
}

static int skeleton_open(struct inode *i, struct file *f){
  pr_info ("skeleton : open operation... major:%d, minor:%d\n", imajor(i), iminor(i));
  return 0;
}

static int skeleton_release (struct inode *i, struct file *f){
  return 0;
}

// Module init function
static int __init skeleton_init(void){
  int i;
  pr_info ("** Linux GPIO IRQ module loaded **\n");

  // Memory allocation
  mydevice_miscdev_array = kzalloc(instances * sizeof(struct miscdevice), GFP_KERNEL);
  if(mydevice_miscdev_array == NULL){
    pr_info("ERROR : Could not allocate devices");
    return 0;
  }

  for(i=0; i<instances; i++){
    char name_buff[128];
    // Misc device register
    mydevice_miscdev_array[i].minor = MISC_DYNAMIC_MINOR;
    sprintf(name_buff, "myDevice%d", i);
    mydevice_miscdev_array[i].name = (char*) kzalloc(strlen(name_buff), GFP_KERNEL);
    if(mydevice_miscdev_array[i].name != NULL) strcpy((char*)mydevice_miscdev_array[i].name, name_buff);
    mydevice_miscdev_array[i].fops = &mylist[i].skeleton_fops;
    misc_register(&mydevice_miscdev_array[i]);
    init_waitqueue_head(&mylist[i].queue);

  }

  // Request pins
  gpio_request(K1_io, "sysfs");
  gpio_request(K2_io, "sysfs");
  gpio_request(K3_io, "sysfs");

  irqNumber_k1 = gpio_to_irq(K1_io);
  irqNumber_k2 = gpio_to_irq(K2_io);
  irqNumber_k3 = gpio_to_irq(K3_io);
  if(irqNumber_k1 == 0 || irqNumber_k2 == 0 || irqNumber_k3 == 0){
    pr_info("ERROR :could not get GPIO IRQ number\n");
    return 0;
  }
  else{
    pr_info("GPIO IRQ number : %d, %d, %d\n", irqNumber_k1, irqNumber_k2, irqNumber_k3);
  }

  resIRQk1 = request_irq(
    irqNumber_k1,
    (irq_handler_t) gpio_irq_handler_k1,
    IRQF_TRIGGER_RISING,
    "mymoduleGPIOirqK1",
    NULL
  );
  
  resIRQk2 = request_irq(
    irqNumber_k2,
    (irq_handler_t) gpio_irq_handler_k2,
    IRQF_TRIGGER_RISING,
    "mymoduleGPIOirqK2",
    NULL
  );

  resIRQk3 = request_irq(
    irqNumber_k3,
    (irq_handler_t) gpio_irq_handler_k3,
    IRQF_TRIGGER_RISING,
    "mymoduleGPIOirqK3",
    NULL
  );

  if(resIRQk1 != 0 || resIRQk2 != 0 || resIRQk3 != 0){
    pr_info("ERROR : One of IRQs already in use\n");
    return 0;
  }
  

  return 0;
}

// Module exit function
static void __exit skeleton_exit(void){
  int i;

  pr_info ("** Linux GPIO IRQ module unloading **\n");

  // Free devices
  if(mydevice_miscdev_array != NULL){
    for(i=0; i<instances; i++){
      misc_deregister(&mydevice_miscdev_array[i]);
    }
    kfree(mydevice_miscdev_array);
  }

  // Free pins
  gpio_free(K1_io);
  gpio_free(K2_io);
  gpio_free(K3_io);

  // Free GPIO IRQ
  if(resIRQk1 == 0) free_irq(irqNumber_k1, NULL);
  if(resIRQk2 == 0) free_irq(irqNumber_k2, NULL);
  if(resIRQk3 == 0) free_irq(irqNumber_k3, NULL);

  pr_info ("** Linux GPIO IRQ module unloading done **\n");
}

module_init (skeleton_init);
module_exit (skeleton_exit);

MODULE_AUTHOR ("Dan Yvan Baumgartner <dan.baumgartner@master.hes-so.ch>");
MODULE_DESCRIPTION ("Module skeleton");
MODULE_LICENSE ("GPL");


