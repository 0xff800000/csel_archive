/* skeleton.c for char pilote */
#include <linux/module.h> /* modules */
#include <linux/init.h> /* macros */
#include <linux/kernel.h> /* debugging */
#include <linux/string.h> /* handles strings */
#include <linux/list.h> /* listes */
#include <linux/slab.h>
#include <linux/gfp.h>
#include <linux/moduleparam.h> /* kernel parameters */
#include <linux/io.h> /* entrées/sorties */
#include <linux/uaccess.h> /* copie entre noyau et userspace */
#include <linux/fs.h> /* operation du pilote */
#include <linux/cdev.h>

/* specific to sysfs*/
#include <linux/device.h>
#include <linux/platform_device.h>

dev_t skeleton_dev;
static struct cdev skeleton_cdev;


static ssize_t skeleton_read ( struct file *f,
			       char __user *buf,
			       size_t count,
			       loff_t *off);
static ssize_t skeleton_write ( struct file *f,
				const char __user *buf,
				size_t count,
				loff_t *off);
static int skeleton_open (struct inode *i,
			  struct file *f);
static int skeleton_release (struct inode *i,
			     struct file *f);


static char s_buffer[1000];

static unsigned int instances = 5; /* nombre d'instance à créer*/


static struct file_operations skeleton_fops =
  {
   .owner = THIS_MODULE,
   .open = skeleton_open,
   .read = skeleton_read,
   .write = skeleton_write,
   .release = skeleton_release,
  };


// exemple open function
static int skeleton_open (struct inode *i, struct file *f)
{
  pr_info ("skeleton : open operation... major:%d, minor:%d\n", imajor(i), iminor(i));

  if((f->f_mode & (FMODE_READ | FMODE_WRITE)) != 0)
    {
      pr_info ("skeleton: opened for reading and writing...\n");
    }
  else if ((f->f_mode & FMODE_READ) != 0)
    {
      pr_info ("skeleton: opened for reading...\n");
    }
  else if ((f->f_mode & FMODE_WRITE) != 0)
    {
      pr_info ("skeleton: opened for writing...\n");
    }

  return 0;
}


// ##########################
// exemple read function
static ssize_t
skeleton_read( struct file *f,
	       char __user *buf,
	       size_t count,
	       loff_t *off)
{
  // compute remaining bytes to copy, update count and pointers
  ssize_t remaining = strlen(s_buffer) - (ssize_t)(*off);
  char* ptr = s_buffer + *off;

  if(count > remaining) count = remaining;
  *off += count;

  //copy required number of bytes
  if(copy_to_user (buf, ptr, count) != 0) count = -EFAULT;

  pr_info("skeleton: read operation... read=%ld\n", count);

  return count;
}


// ##########################
// exemple write function
static ssize_t
skeleton_write( struct file *f,
	       const char __user *buf,
	       size_t count,
	       loff_t *off)
{
  // compute remaining bytes to copy, update count and pointers
  ssize_t remaining = sizeof(s_buffer) - (ssize_t)(*off);
  char* ptr = s_buffer + *off;

  *off += count;

  if(count >= remaining) count = -EIO;

  if(count > 0)
    {
      ptr[count] = 0;
      if(copy_from_user (ptr, buf, count)) count = -EFAULT;
    }

  pr_info("skeleton: write operation... written=%ld\n", count);

  return count;
}

static int skeleton_release (struct inode *i, struct file *f)
{
  pr_info ("skeleton: release operation...\n");

  return 0;
}



/* Module init function */
static int __init skeleton_init(void)
{
  int status = alloc_chrdev_region (&skeleton_dev, 0, instances, "mymoduleDriver");
  if (status == 0)
    {
      cdev_init (&skeleton_cdev, &skeleton_fops);
      skeleton_cdev.owner = THIS_MODULE;
      status = cdev_add (&skeleton_cdev, skeleton_dev, instances);
    }
  pr_info ("** Linux driver device skeleton loaded **\n");
  pr_info ("** Driver created with %d device(s) **\n", instances);

  return status;
}

/* Module exit function */
static void __exit skeleton_exit(void)
{
  cdev_del (&skeleton_cdev);
  unregister_chrdev_region (skeleton_dev, instances);
  pr_info ("** Linux driver device skeleton unloaded **\n");
}

module_init (skeleton_init);
module_exit (skeleton_exit);

MODULE_AUTHOR ("Vincent Conus <vincent.conus@master.hes-so.ch>");
MODULE_DESCRIPTION ("Driver module skeleton");
MODULE_LICENSE ("GPL");
