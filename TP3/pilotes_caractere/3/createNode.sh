#! /bin/sh

if [ "$*" == "" ];then
    echo 'Usage : ./createNode.sh <n nodes>'
    exit 1
fi

echo 'Removing previous nodes'
rm /dev/mymoduleDriver*

majorNumber=$(sed -n 's/mymoduleDriver//p' /proc/devices)
if [ "$majorNumber" == "" ];then
    echo 'Error : Major number not found'
    exit 1
else
    echo "Major number : $majorNumber"
fi

echo 'Creating nodes files'
for x in $(seq 0 $1)
do
    echo mknod /dev/mymoduleDriver$x c $majorNumber $x
    mknod /dev/mymoduleDriver$x c $majorNumber $x
done

