#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/list.h>
#include <linux/slab.h>
#include <linux/gfp.h>
#include <linux/moduleparam.h>
#include <linux/io.h>
#include <linux/uaccess.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/platform_device.h>

dev_t skeleton_dev;
static struct cdev skeleton_cdev;

static ssize_t skeleton_read(struct file *f, char __user *buf, size_t count, loff_t *off);
static ssize_t skeleton_write(struct file *f, const char __user *buf, size_t count, loff_t *off);
static int skeleton_open(struct inode *i, struct file *f);
static int skeleton_release(struct inode *i, struct file *f);

struct list_element{
  int id;
  char*str;
  struct list_head node; // Kernel list structure
};

static LIST_HEAD (mylinked_list);

static unsigned int current_minor;

static unsigned int instances = 5;
module_param(instances, int, 0);
static unsigned int buffer_size = 128;
module_param(buffer_size, int, 0);

static struct file_operations skeleton_fops ={
  .owner = THIS_MODULE,
  .open = skeleton_open,
  .read = skeleton_read,
  .write = skeleton_write,
  .release = skeleton_release,
};

static int skeleton_open(struct inode *i, struct file *f){
  pr_info ("skeleton : open operation... major:%d, minor:%d\n", imajor(i), iminor(i));
  current_minor = iminor(i);

  if((f->f_mode & (FMODE_READ | FMODE_WRITE)) != 0){
    pr_info ("skeleton: opened for reading and writing...\n");
  }
  else if ((f->f_mode & FMODE_READ) != 0){
    pr_info ("skeleton: opened for reading...\n");
  }
  else if ((f->f_mode & FMODE_WRITE) != 0){
    pr_info ("skeleton: opened for writing...\n");
  }

  return 0;
}

static ssize_t skeleton_read(struct file *f, char __user *buf, size_t count, loff_t *off){
  struct list_element* ele;
  char*ele_str = NULL;
  char* ptr;
  struct list_head * node;
  ssize_t remaining;

  list_for_each(node, &mylinked_list){
    ele = list_entry(node, struct list_element, node);
    if(ele != NULL){
      if(ele->id == current_minor){
        pr_info("skeleton: writing element id:%d\n", ele->id);
        ele_str = ele->str;
        break;
      }
    }
  }

  if(ele_str == NULL) return 0;

  // compute remaining bytes to copy, update count and pointers
  remaining = strlen(ele_str) - (ssize_t)(*off);
  ptr = ele_str + *off;

  if(count > remaining) count = remaining;
  *off += count;

  //copy required number of bytes
  if(copy_to_user (buf, ptr, count) != 0) count = -EFAULT;

  pr_info("skeleton: read operation... read=%ld\n", count);

  return count;
}


static ssize_t skeleton_write(struct file *f, const char __user *buf, size_t count, loff_t *off){
  struct list_element* ele;
  char*ele_str = NULL;
  char* ptr;
  struct list_head * node;
  ssize_t remaining;

  list_for_each(node, &mylinked_list){
    ele = list_entry(node, struct list_element, node);
    if(ele != NULL){
      if(ele->id == current_minor){
        pr_info("skeleton: writing element id:%d\n", ele->id);
        ele_str = ele->str;
        break;
      }
    }
  }

  if(ele_str == NULL) return 0;

  // compute remaining bytes to copy, update count and pointers
  remaining = sizeof(ele_str) - (ssize_t)(*off);
  ptr = ele_str + *off;

  *off += count;

  if(count >= remaining) count = -EIO;

  if(count > 0){
    ptr[count] = 0;
    if(copy_from_user (ptr, buf, count)) count = -EFAULT;
  }

  pr_info("skeleton: write operation... written=%ld\n", count);

  return count;
}

static int skeleton_release (struct inode *i, struct file *f){
  pr_info ("skeleton: release operation...\n");

  return 0;
}


static int __init skeleton_init(void){
  // Variables
  int i;
  struct list_element* ele;
  char*ele_str;

  // Create devices
  int status = alloc_chrdev_region (&skeleton_dev, 0, instances, "mymoduleDriver");
  if (status == 0){
    cdev_init (&skeleton_cdev, &skeleton_fops);
    skeleton_cdev.owner = THIS_MODULE;
    status = cdev_add (&skeleton_cdev, skeleton_dev, instances);
  }

  // Memory allocation
  for(i=0; i<instances; i++){
    ele = kzalloc(sizeof(struct list_element), GFP_KERNEL);
    if(ele != NULL){
      ele->id = i;
      ele_str = (char*) kzalloc(buffer_size, GFP_KERNEL);
      if(ele_str != NULL){
        ele->str = ele_str;
      }
      list_add(&ele->node, &mylinked_list);
    }
  }
  pr_info ("** Linux driver device skeleton loaded **\n");
  pr_info ("** Driver created with %d device(s) **\n", instances);

  return status;
}

static void __exit skeleton_exit(void){
  struct list_element * ele;
  struct list_element * tmp;
  // Free memory
  list_for_each_entry_safe(ele, tmp, &mylinked_list, node){
    list_del(&ele->node);
    kfree(ele->str);
    kfree(ele);
  }

  // Free devices
  cdev_del (&skeleton_cdev);
  unregister_chrdev_region (skeleton_dev, instances);
  pr_info ("** Linux driver device skeleton unloaded **\n");
}

module_init (skeleton_init);
module_exit (skeleton_exit);

MODULE_AUTHOR ("Dan Yvan Baumgartner <dan.baumgartner@master.hes-so.ch>");
MODULE_DESCRIPTION ("Driver module skeleton");
MODULE_LICENSE ("GPL");
