#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

#define FILESIZE 4096
#define DEVICE_ADDR 0x1c14000

int main(int argc, char *argv[])
{
		int fd = open("/dev/mem", O_RDONLY);
		if (fd == -1) {
			perror("Error opening file for writing");
			exit(EXIT_FAILURE);
		}

		uint32_t* map = mmap(NULL, FILESIZE, PROT_READ, MAP_SHARED, fd, DEVICE_ADDR);
		if (map == MAP_FAILED) {
			close(fd);
			perror("Error mmapping the file");
			exit(EXIT_FAILURE);
		}

		uint32_t chipid[4]={
			[0] = map[0x200/sizeof(uint32_t)],
			[1] = map[0x204/sizeof(uint32_t)],
			[2] = map[0x208/sizeof(uint32_t)],
			[3] = map[0x20c/sizeof(uint32_t)]
		};

		printf("Value : ");
		for(int i=0;i<4;i++){
			printf("%x", chipid[i]);
		}
		printf("\n");

		if (munmap(map, FILESIZE) == -1) {
			perror("Error un-mmapping the file");
		}

		close(fd);
		return 0;
}
