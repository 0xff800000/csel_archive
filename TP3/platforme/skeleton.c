#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/list.h>
#include <linux/slab.h>
#include <linux/gfp.h>
#include <linux/moduleparam.h>
#include <linux/io.h>
#include <linux/uaccess.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/miscdevice.h>
#include <linux/platform_device.h>
#include <linux/of_platform.h>
#include <linux/of_device.h>

dev_t skeleton_dev;

static ssize_t skeleton_read(struct file *f, char __user *buf, size_t count, loff_t *off);
static ssize_t skeleton_write(struct file *f, const char __user *buf, size_t count, loff_t *off);
static int skeleton_open(struct inode *i, struct file *f);
static int skeleton_release(struct inode *i, struct file *f);

static struct file_operations skeleton_fops ={
  .owner = THIS_MODULE,
  .open = skeleton_open,
  .read = skeleton_read,
  .write = skeleton_write,
  .release = skeleton_release,
};

struct list_element{
  int id;
  char*str;
  struct list_head node; // Kernel list structure
};

static LIST_HEAD (mylinked_list);

static unsigned int current_minor;

static unsigned int instances = 1;
static unsigned int buffer_size = 128;
module_param(buffer_size, int, 0);

static struct miscdevice * mydevice_miscdev_array;

static int skeleton_open(struct inode *i, struct file *f){
  pr_info ("skeleton : open operation... major:%d, minor:%d\n", imajor(i), iminor(i));
  current_minor = iminor(i);

  if((f->f_mode & (FMODE_READ | FMODE_WRITE)) != 0){
    pr_info ("skeleton: opened for reading and writing...\n");
  }
  else if ((f->f_mode & FMODE_READ) != 0){
    pr_info ("skeleton: opened for reading...\n");
  }
  else if ((f->f_mode & FMODE_WRITE) != 0){
    pr_info ("skeleton: opened for writing...\n");
  }

  return 0;
}

static ssize_t skeleton_read(struct file *f, char __user *buf, size_t count, loff_t *off){
  struct list_element* ele;
  char*ele_str = NULL;
  char* ptr;
  struct list_head * node;
  ssize_t remaining;

  list_for_each(node, &mylinked_list){
    ele = list_entry(node, struct list_element, node);
    if(ele != NULL){
      if(ele->id == current_minor){
        pr_info("skeleton: writing element id:%d\n", ele->id);
        ele_str = ele->str;
        break;
      }
    }
  }

  if(ele_str == NULL) return 0;

  // compute remaining bytes to copy, update count and pointers
  remaining = strlen(ele_str) - (ssize_t)(*off);
  ptr = ele_str + *off;

  if(count > remaining) count = remaining;
  *off += count;

  //copy required number of bytes
  if(copy_to_user (buf, ptr, count) != 0) count = -EFAULT;

  pr_info("skeleton: read operation... read=%ld\n", count);

  return count;
}


static ssize_t skeleton_write(struct file *f, const char __user *buf, size_t count, loff_t *off){
  struct list_element* ele;
  char*ele_str = NULL;
  char* ptr;
  struct list_head * node;
  ssize_t remaining;

  list_for_each(node, &mylinked_list){
    ele = list_entry(node, struct list_element, node);
    if(ele != NULL){
      if(ele->id == current_minor){
        pr_info("skeleton: writing element id:%d\n", ele->id);
        ele_str = ele->str;
        break;
      }
    }
  }

  if(ele_str == NULL) return 0;

  // compute remaining bytes to copy, update count and pointers
  remaining = sizeof(ele_str) - (ssize_t)(*off);
  ptr = ele_str + *off;

  *off += count;

  if(count >= remaining) count = -EIO;

  if(count > 0){
    ptr[count] = 0;
    if(copy_from_user (ptr, buf, count)) count = -EFAULT;
  }

  pr_info("skeleton: write operation... written=%ld\n", count);

  return count;
}

static int skeleton_release (struct inode *i, struct file *f){
  pr_info ("skeleton: release operation...\n");

  return 0;
}


static int __init skeleton_init(void){
  // Variables
  int i;
  struct list_element* ele;
  const char *prop_str = 0;

  // Read device tree
  struct device_node *dt_node = of_find_node_by_path("/mydevice");
  if(dt_node){
    int ret = 0;
    const unsigned int *prop_reg = 0;
    pr_info("skeleton : Reading device tree\n");
    prop_reg = of_get_property(dt_node, "reg", NULL);
    if(prop_reg != 0){
      unsigned long reg = of_read_ulong(prop_reg, 1);
      pr_info("skeleton : instances=%lu\n",reg);
      instances = reg;
    }

    ret = of_property_read_string(dt_node, "attribute", &prop_str);
    if((prop_str != 0) && (ret == 0)){
      pr_info("skeleton : device name=%s (ret=%d)\n", prop_str, ret);
    }
  }
  else{
    pr_info("ERROR : could not read device tree");
  }

  // Memory allocation
  mydevice_miscdev_array = kzalloc(instances * sizeof(struct miscdevice), GFP_KERNEL);
  if(mydevice_miscdev_array == NULL){
    pr_info("ERROR : Could not allocate devices");
    return 0;
  }

  for(i=0; i<instances; i++){
    char name_buff[128];
    // Misc device register
    mydevice_miscdev_array[i].minor = MISC_DYNAMIC_MINOR;
    if(prop_str != NULL){
      sprintf(name_buff, "%s%d", prop_str, i);
    }
    else{
      sprintf(name_buff, "myMiscDevice%d", i);
    }
    mydevice_miscdev_array[i].name = (char*) kzalloc(strlen(name_buff), GFP_KERNEL);
    if(mydevice_miscdev_array[i].name != NULL) strcpy((char*)mydevice_miscdev_array[i].name, name_buff);
    mydevice_miscdev_array[i].fops = &skeleton_fops;
    misc_register(&mydevice_miscdev_array[i]);

    // Create list
    ele = kzalloc(sizeof(struct list_element), GFP_KERNEL);
    if(ele != NULL){
      char*ele_str;
      ele->id = mydevice_miscdev_array[i].minor;
      ele_str = (char*) kzalloc(buffer_size, GFP_KERNEL);
      if(ele_str != NULL){
        ele->str = ele_str;
      }
      list_add(&ele->node, &mylinked_list);
    }
  }
  pr_info ("** Linux driver device skeleton loaded **\n");
  pr_info ("** Driver created with %d device(s) **\n", instances);

  return 0;
  // return status;
}

static void __exit skeleton_exit(void){
  struct list_element * ele;
  struct list_element * tmp;
  int i;
  // Free devices
  if(mydevice_miscdev_array != NULL){
    for(i=0; i<instances; i++){
      misc_deregister(&mydevice_miscdev_array[i]);
    }
    kfree(mydevice_miscdev_array);
  }

  // Free memory
  list_for_each_entry_safe(ele, tmp, &mylinked_list, node){
    list_del(&ele->node);
    kfree(ele->str);
    kfree(ele);
  }

  pr_info ("** Linux driver device skeleton unloaded **\n");
}

module_init (skeleton_init);
module_exit (skeleton_exit);

MODULE_AUTHOR ("Dan Yvan Baumgartner <dan.baumgartner@master.hes-so.ch>");
MODULE_DESCRIPTION ("Driver module skeleton");
MODULE_LICENSE ("GPL");
