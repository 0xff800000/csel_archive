#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/list.h>
#include <linux/slab.h>
#include <linux/gfp.h>
#include <linux/moduleparam.h>
#include <linux/io.h>
#include <linux/uaccess.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/platform_device.h>

struct sysfs_block{
  char attr1_buf[1000];
  char attr2_buf[1000];
};

static struct sysfs_block block;

static ssize_t skeleton_show_attr1(struct device *dev, struct device_attribute *attr, char *buf){
  strcpy(buf, block.attr1_buf);
  pr_info("sysfs : attr1 read\n");
  return strlen(block.attr1_buf);
}

static ssize_t skeleton_store_attr1(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
  int len = sizeof(block.attr1_buf) - 1;
  if(len > count) len = count;
  strncpy(block.attr1_buf, buf, len);
  block.attr1_buf[len] = 0;
  pr_info("sysfs : attr1 written\n");
  return len;
}

static ssize_t skeleton_show_attr2(struct device *dev, struct device_attribute *attr, char *buf){
  strcpy(buf, block.attr2_buf);
  pr_info("sysfs : attr2 read\n");
  return strlen(block.attr2_buf);
}

static ssize_t skeleton_store_attr2(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
  int len = sizeof(block.attr2_buf) - 1;
  if(len > count) len = count;
  strncpy(block.attr2_buf, buf, len);
  block.attr2_buf[len] = 0;
  pr_info("sysfs : attr2 written\n");
  return len;
}

static ssize_t skeleton_show_all(struct device *dev, struct device_attribute *attr, char *buf){
  sprintf(buf, "%s %s", block.attr1_buf, block.attr2_buf);
  pr_info("sysfs : all structure read\n");
  return strlen(buf);
}

static ssize_t skeleton_store_all(struct device *dev, struct device_attribute *attr, const char *buf, size_t count){
  int len1,len2,i;
  for(i=0; i<count; i++) if(buf[i] == ' ') break;
  len1 = (i<sizeof(block.attr1_buf))? i : sizeof(block.attr1_buf);
  len2 = (strlen(buf+i+1)<sizeof(block.attr2_buf))? strlen(buf+i+1) : sizeof(block.attr2_buf);

  strncpy(block.attr1_buf, buf, len1);
  strncpy(block.attr2_buf, buf+len1+1, len2);
  block.attr1_buf[len1+1] = 0;
  block.attr2_buf[len2+1] = 0;

  pr_info("sysfs : all structure written\n");
  return len1 + len2 + 1;
}

DEVICE_ATTR(attr1, 0664, skeleton_show_attr1, skeleton_store_attr1);
DEVICE_ATTR(attr2, 0664, skeleton_show_attr2, skeleton_store_attr2);
DEVICE_ATTR(all, 0664, skeleton_show_all, skeleton_store_all);

static void sysfs_dev_release(struct device *dev){
  
}

static struct platform_driver sysfs_driver = {
  .driver = {
    .name = "skeleton",
  },
};

static struct platform_device sysfs_device = {
  .name = "skeleton",
  .id = -1,
  .dev.release = sysfs_dev_release,
};

static int __init skeleton_init(void){
  // sysfs
  int status = 0;
  if(status == 0){
    status = platform_driver_register(&sysfs_driver);
  }
  if(status == 0){
    status = platform_device_register(&sysfs_device);
  }
  if(status == 0){
    device_create_file(&sysfs_device.dev, &dev_attr_attr1);
    device_create_file(&sysfs_device.dev, &dev_attr_attr2);
    device_create_file(&sysfs_device.dev, &dev_attr_all);
  }
  pr_info ("** Linux driver sysfs skeleton loaded **\n");
  return status;
}

static void __exit skeleton_exit(void){
  // sysfs
  device_remove_file(&sysfs_device.dev, &dev_attr_attr1);
  device_remove_file(&sysfs_device.dev, &dev_attr_attr2);
  device_remove_file(&sysfs_device.dev, &dev_attr_all);
  platform_device_unregister(&sysfs_device);
  platform_driver_unregister(&sysfs_driver);

  pr_info ("** Linux driver sysfs skeleton unloaded **\n");
}

module_init (skeleton_init);
module_exit (skeleton_exit);

MODULE_AUTHOR ("Dan Yvan Baumgartner <dan.baumgartner@master.hes-so.ch>");
MODULE_DESCRIPTION ("Driver module skeleton");
MODULE_LICENSE ("GPL");
