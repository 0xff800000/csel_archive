/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Project:	HEIA-FR / HES-SO MSE - MA-CSEL1 Laboratory
 *
 * Abstract: System programming -  file system
 *
 * Purpose:	NanoPi silly status led control system
 *
 * Autĥor:	Daniel Gachet
 * Date:	07.11.2018
 *
 *
 * Modifs: Baumgartner Dan Yvan & Conus Vincent
 *
 *
 */
#include <sys/types.h>
#include <sys/stat.h>

/* epoll and timerfd */
#include <sys/epoll.h>
#include <sys/timerfd.h>

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <syslog.h>


/*
 * K1 switch  ---> gpio0
 * K2 switch  ---> gpio2
 * K3 switch  ---> gpio3
 * status led - gpioa.10 --> gpio10
 * power led  - gpiol.10 --> gpio362
 */
#define GPIO_EXPORT	"/sys/class/gpio/export"
#define GPIO_UNEXPORT	"/sys/class/gpio/unexport"
#define GPIO_LED	"/sys/class/gpio/gpio10"
#define LED	  "10"

#define GPIO_K1		"/sys/class/gpio/gpio0"
#define K1				"0"
#define GPIO_K2		"/sys/class/gpio/gpio2"
#define K2				"2"
#define GPIO_K3		"/sys/class/gpio/gpio3"
#define K3				"3"

#define MAX_EVENTS 4

int led;
int tfd;
struct itimerspec timerValue;


static int open_led(){
	// unexport pin out of sysfs (reinitialization)
	int f = open (GPIO_UNEXPORT, O_WRONLY);
	write (f, LED, strlen(LED));
	close (f);

	// export pin to sysfs
	f = open (GPIO_EXPORT, O_WRONLY);
	write (f, LED, strlen(LED));
	close (f);	

	// config pin
	f = open (GPIO_LED "/direction", O_WRONLY);
	write (f, "out", 3);
	close (f);

	// open gpio value attribute
 	f = open (GPIO_LED "/value", O_RDWR);
	return f;
}

/* One struct to rule 'em all */
enum{eIncrease, eDecrease, eReset};
struct event_ctl{
  int fd;
  void (*process)(struct event_ctl *event);
  int function;
};

void set_time(){
	if(
		timerfd_settime(
			tfd,
			TFD_TIMER_ABSTIME,
			&timerValue,
			NULL
		)
	){
		perror(" ---> timerfd_settime error");
		exit(-1);
	}
	syslog(LOG_INFO, "Period changed to %d second(s)", timerValue.it_interval.tv_sec);
}

void increase_timer_freq(){
	printf("%s\n", __FUNCTION__);
	timerValue.it_value.tv_sec++;
	timerValue.it_interval.tv_sec++;
	set_time();
}

void decrease_timer_freq(){
	printf("%s\n", __FUNCTION__);
	int t1 = timerValue.it_value.tv_sec;
	timerValue.it_value.tv_sec = (t1 > 1)? t1-1 : 1;
	int t2 = timerValue.it_interval.tv_sec--;
	timerValue.it_interval.tv_sec = (t2 > 1)? t2-1 : 1;
	set_time();
}

void reset_timer_freq(){
	printf("%s\n", __FUNCTION__);
	timerValue.it_value.tv_sec = 1;
	timerValue.it_value.tv_nsec = 0;
	timerValue.it_interval.tv_sec = 1;
	timerValue.it_interval.tv_nsec = 0;
	set_time();
}

void process_timer(struct event_ctl *event){
	uint64_t buf;
	read(event->fd, &buf, sizeof(uint64_t));
	printf("process_timer() : event on fd = %d after %d sec\n",
		event->fd,
		timerValue.it_interval.tv_sec
	);
	static int led_state = 0;
	// switch the state of the led
	if (led_state == 0){
		pwrite (led, "1", sizeof("1"), 0); // write 1 to the led
		led_state = 1;
	}
	else{
		pwrite (led, "0", sizeof("1"), 0); // write 0 to the led
		led_state = 0;	
	}
}

void process_button(struct event_ctl *event){
	char buf;
	lseek(event->fd, 0, SEEK_SET);
	read(event->fd, &buf, 1);
	printf("process_button() : event on fd = %d value=%c\n",
		event->fd,
		buf
	);
	if(buf == '1'){
		switch(event->function){
			case eIncrease:
				increase_timer_freq();
				break;
			case eDecrease:
				decrease_timer_freq();
				break;
			case eReset:
				reset_timer_freq();
				break;
		}
	}
}

struct button{
	char* gpio_path;
	char* gpio_pin;
	int fd;
	int function;
};

int configure_button(struct button* sw, int len){
	int ret = -1;
	for(int i=0; i<len; i++){
		char path[256];
		// unexport pin out of sysfs (reinitialization)
		int f = open (GPIO_UNEXPORT, O_WRONLY);
		write (f, sw[i].gpio_pin, strlen(sw[i].gpio_pin));
		if(f < 0){
			perror(" ---> Unexport failed");
			exit(-1);
		}
		close (f);

		// export pin to sysfs
		f = open (GPIO_EXPORT, O_WRONLY);
		if(f < 0){
			perror(" ---> Export failed");
			exit(-1);
		}
		write (f, sw[i].gpio_pin, strlen(sw[i].gpio_pin));
		close (f);

		// config pin
		strcpy(path, (const char*)sw[i].gpio_path);
		strcat(path, "/direction");
		f = open (path, O_WRONLY);
		if(f < 0){
			perror(" ---> Set direction failed");
			exit(-1);
		}
		write (f, "in", 2);
		close (f);
		strcpy(path, (const char*)sw[i].gpio_path);
		strcat(path, "/edge");
		f = open (path, O_WRONLY);
		if(f < 0){
			perror(" ---> Set edge failed");
			exit(-1);
		}
		write (f, "both", 4);
		close (f);

		// open gpio value attribute
		strcpy(path, (const char*)sw[i].gpio_path);
		strcat(path, "/value");
 		f = open (path, O_RDWR);
		if(f < 0){
			perror(" ---> Open value failed");
			exit(-1);
		}
		sw[i].fd = f;
		if(f > ret) ret = f;
	}
	return ret;
}

int main(int argc, char* argv[]) {
	openlog("MUX LED ctrl", LOG_CONS, LOG_USER);

	/* events structures */
	struct epoll_event event_timer;
	struct event_ctl events_ctl[MAX_EVENTS];

	/* Create time file descriptor */
	tfd = timerfd_create(CLOCK_REALTIME, 0);
	if (tfd == -1){
		perror(" ---> Error with timerfd");
		exit(-1);
	}
	printf("Timer fd=%d\n",tfd);
	events_ctl[0].fd = tfd;
	events_ctl[0].process = process_timer;

	bzero(&timerValue, sizeof(timerValue));
	reset_timer_freq();

	/* Création du contexte epoll*/
	int epfd = epoll_create1(0);
	if (epfd == -1){
		perror(" ---> Error when starting epoll");
		exit(-1);
	}

	event_timer.events = EPOLLIN | EPOLLET; // only for input and rising edge
	event_timer.data.fd = tfd;
	event_timer.data.ptr = &events_ctl[0];

	/* add the timerfd to epoll */
	if (epoll_ctl(epfd, EPOLL_CTL_ADD, tfd, &event_timer)){
		perror(" ---> epoll ctl error");
		exit(-1);
	}


	/* set timer */
	set_time();

	/* LEDs setup */
	led = open_led(); // open fd for the LEDs


	/* Buttons */
	/* Init number buttons */
	struct button buttons[] = {
		[0] = {
			.gpio_path = GPIO_K1,
			.gpio_pin = K1,
			.fd = 0,
			.function = eIncrease,
		},
		[1] = {
			.gpio_path = GPIO_K2,
			.gpio_pin = K2,
			.fd = 0,
			.function = eReset,
		},
		[2] = {
			.gpio_path = GPIO_K3,
			.gpio_pin = K3,
			.fd = 0,
			.function = eDecrease,
		}
	};
	configure_button(buttons, sizeof(buttons)/sizeof(struct button));

	struct epoll_event events[MAX_EVENTS] = {0};
	for(int i=0; i<sizeof(buttons)/sizeof(struct button); i++){
		events[i].events = EPOLLET;
		events[i].data.fd = buttons[i].fd;
		events[i].data.ptr = &events_ctl[i+1];

		events_ctl[i+1].fd = buttons[i].fd;
		events_ctl[i+1].process = process_button;
		events_ctl[i+1].function = buttons[i].function;

		/* add the buttons to the epoll (fd and event list)*/
		int ret = epoll_ctl(epfd, EPOLL_CTL_ADD, buttons[i].fd, &events[i]);

		printf("Register %d\n", buttons[i].fd);

		if(ret < 0){
			perror("Epoll init ctl");
			exit(-1);
		}
	}



	/***********************************/
	// final loop
	while(1){
		int nr = epoll_wait (epfd, events, MAX_EVENTS, -1);
		if(nr == -1){
			perror(" ---> epoll error");
			exit(-1);
		}
		else if (nr == 0){perror(" ---> nothing received!");}
		else {
			for (int i=0; i<nr; i++){
				struct event_ctl* event = events[i].data.ptr;
				if(event->process != NULL) event->process(event);
			}
		}
	}


	return 0;

}

