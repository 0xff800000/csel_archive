/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / HES-SO MSE - MA-CSEL1 Laboratory
 *
 * Abstract: System programming -  file system
 *
 * Purpose:	NanoPi silly status led control system
 *
 * Autĥor:	Daniel Gachet
 * Date:	07.11.2018
 */
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/epoll.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

/*
 * K1 switch  ---> gpio0
 * K2 switch  ---> gpio2
 * K3 switch  ---> gpio3
 * status led - gpioa.10 --> gpio10
 * power led  - gpiol.10 --> gpio362
 */

#define GPIO_EXPORT		"/sys/class/gpio/export"
#define GPIO_UNEXPORT	"/sys/class/gpio/unexport"
#define GPIO_LED		"/sys/class/gpio/gpio10"
#define LED				"10"

#define GPIO_K1		"/sys/class/gpio/gpio0"
#define K1				"0"
#define GPIO_K2		"/sys/class/gpio/gpio2"
#define K2				"2"
#define GPIO_K3		"/sys/class/gpio/gpio3"
#define K3				"3"

static int open_led()
{
	// unexport pin out of sysfs (reinitialization)
	int f = open (GPIO_UNEXPORT, O_WRONLY);
	write (f, LED, strlen(LED));
	close (f);

	// export pin to sysfs
	f = open (GPIO_EXPORT, O_WRONLY);
	write (f, LED, strlen(LED));
	close (f);

	// config pin
	f = open (GPIO_LED "/direction", O_WRONLY);
	write (f, "out", 3);
	close (f);

	// open gpio value attribute
 	f = open (GPIO_LED "/value", O_RDWR);
	return f;
}

struct button{
	char* gpio_path;
	char* gpio_pin;
	int fd;
};

void process_event(int fd);

int configure_button(struct button* sw, int len){
	int ret = -1;
	for(int i=0; i<len; i++){
		char path[256];
		// unexport pin out of sysfs (reinitialization)
		int f = open (GPIO_UNEXPORT, O_WRONLY);
		write (f, sw[i].gpio_pin, strlen(sw[i].gpio_pin));
		if(f < 0){
			perror("Unexport failed");
			exit(-1);
		}
		close (f);

		// export pin to sysfs
		f = open (GPIO_EXPORT, O_WRONLY);
		if(f < 0){
			perror("Export failed");
			exit(-1);
		}
		write (f, sw[i].gpio_pin, strlen(sw[i].gpio_pin));
		close (f);

		// config pin
		strcpy(path, (const char*)sw[i].gpio_path);
		strcat(path, "/direction");
		f = open (path, O_WRONLY);
		if(f < 0){
			perror("Set direction failed");
			exit(-1);
		}
		write (f, "in", 2);
		close (f);
		strcpy(path, (const char*)sw[i].gpio_path);
		strcat(path, "/edge");
		f = open (path, O_WRONLY);
		if(f < 0){
			perror("Set edge failed");
			exit(-1);
		}
		write (f, "both", 4);
		close (f);

		// open gpio value attribute
		strcpy(path, (const char*)sw[i].gpio_path);
		strcat(path, "/value");
 		f = open (path, O_RDONLY);
		if(f < 0){
			perror("Open value failed");
			exit(-1);
		}
		sw[i].fd = f;
		if(f > ret) ret = f;
	}
	return ret;
}

void debug_print_button(struct button* sw, int len){
	for(int i=0; i<len; i++){
		char buff = 0;
		pread(sw[i].fd,&buff,1,0);
		printf("%c", (int)buff);
	}
	printf("\n");
}


int main(int argc, char* argv[])
{
	long duty = 2;		// %
	long period = 1000; // ms
	if (argc >= 2)
		period   = atoi (argv[1]);
	period *= 1000000; // in ns

	// compute duty period...
	long p1 = period / 100 * duty;
	long p2 = period - p1;

 	int led = open_led();
	pwrite (led, "1", sizeof("1"), 0);

	struct timespec t1;
	clock_gettime (CLOCK_MONOTONIC, &t1);

	// Init number buttons
	struct button buttons[] = {
		[0] = {
			.gpio_path = GPIO_K1,
			.gpio_pin = K1,
			.fd = 0
		},
		[1] = {
			.gpio_path = GPIO_K2,
			.gpio_pin = K2,
			.fd = 0
		},
		[2] = {
			.gpio_path = GPIO_K3,
			.gpio_pin = K3,
			.fd = 0
		}
	};
	configure_button(buttons, sizeof(buttons)/sizeof(struct button));

	// Configure epoll
	struct epoll_event events[3] = {0};
	int epfd = epoll_create1(0);
	if(epfd < 0){
		perror("Epoll create");
		exit(-1);
	}
	for(int i=0; i<sizeof(buttons)/sizeof(struct button); i++){
		events[i].events = EPOLLET;
		events[i].data.fd = buttons[i].fd;
		int ret = epoll_ctl(epfd, EPOLL_CTL_ADD, buttons[i].fd, &events[i]);
		printf("Register %d\n", buttons[i].fd);
		if(ret < 0){
			perror("Epoll init ctl");
			exit(-1);
		}
	}


	int k = 0;
	while(1) {
		struct timespec t2;
		clock_gettime (CLOCK_MONOTONIC, &t2);

		long delta = (t2.tv_sec  - t1.tv_sec) * 1000000000 +
			     (t2.tv_nsec - t1.tv_nsec);

		int toggle = ((k == 0) && (delta >= p1))
			   | ((k == 1) && (delta >= p2));
		if (toggle) {
			t1 = t2;
			k = (k+1)%2;
			if (k == 0)
				pwrite (led, "1", sizeof("1"), 0);
			else
				pwrite (led, "0", sizeof("0"), 0);
		}
		//debug_print_button(buttons, sizeof(buttons)/sizeof(struct button));
		int nr = epoll_wait(epfd, events,3,-1);
		for(int i=0; i<nr; i++){
			printf("event=%d on fd=%d\n", events[i].events, events[i].data.fd);
			usleep(10000);
		}
		usleep(1);
	}

	return 0;
}
