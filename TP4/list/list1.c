#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

void scan_dir(const char* path){
	printf("\n.%s/:\n", path);
	
	DIR* dir = opendir(path);
	if(dir == 0) return;
	while(dir != 0){
		struct dirent* entry = readdir(dir);
		if(entry == 0) break;
		printf("%4d -%s\n", entry->d_type, entry->d_name);
	}
	closedir(dir);
}

void scan_dir_rec(bool recursive, const char* path){
	printf("#########################\n");
	printf("\n.%s/:\n", path);
	
	DIR* dir = opendir(path);
	if(dir == 0) return;
	// Count number of dirs
	int count = 0;
	while(dir != 0){
		struct dirent* entry = readdir(dir);
		if(entry == 0) break;
		count++;
	}
	
	// Get all files in current path
	dir = opendir(path);
	char files[count][256];
	for(int i=0; dir != 0; i++){
		struct dirent* entry = readdir(dir);
		if(entry == 0) break;
		//printf("%4d -%s\n", entry->d_type, entry->d_name);
		strcpy(files[i], entry->d_name);
		printf("%s\n",files[i]);
	}
	closedir(dir);
	if(recursive){
		for(int i=0; i<count; i++){
			char path_rec[256] = {0};
			strcat(path_rec,(const char*)path);
			strcat(path_rec,(const char*)files[i]);
			scan_dir_rec(true,path_rec);
		}
	}
}

int main(int argc, char*argv[]){
	char* path = ".";
	if(argc > 1) path = argv[1];

	//scan_dir(path);
	scan_dir_rec(true,path);
	return 0;
}

