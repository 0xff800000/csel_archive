#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>


void scan_dir(bool recursively, const char* path){
	printf("\n%s/:\n", path);
	
	DIR* dir = opendir(path);
	if(dir == 0) return;
	int count = 0;
	while(dir != 0){
		struct dirent* entry = readdir(dir);
		if(entry == 0) break;
		printf("%4d - %s\n", entry->d_type, entry->d_name);
	}
	closedir(dir);

	// get all file names
	char files[count*256];
	dir = opendir (".");
	if (dir == 0) return;
	int i = 0;
	while (dir != 0) {	
	  struct dirent* entry = readdir (dir);
	  if (entry == 0) break;
	  
	  strcpy (files+i*256, entry->d_name);
	  i++;
	}
	closedir(dir);

	// sort alphabeticaly all files
	qsort (files, count, 256, (int(*)(const void*,const void*))strcmp);

	// process subdirectories if required...
	if (recursively)
	  for (int j=0; j<count; j++) {
	    char* file = files+j*256;
	    if (strcmp (file, "." ) == 0) continue;
	    if (strcmp (file, "..") == 0) continue;
	    
	    struct stat status;
	    int err = stat (file, &status);
	    if (err < 0) continue;
	    if (!S_ISDIR(status.st_mode)) continue;
	    
	    char p[strlen(path)+1+256];
	    strcpy (p, path);
	    strcat (p,"/");
	    strcat (p, file);
	    chdir (file);
	    scan_dir (recursively, p);
	    chdir ("..");
	  }
}

int main(int argc, char*argv){
	char* path = "/home/lmi/git";
	if(argc > 1) path = argv[1];

	scan_dir(true, path);
	return 0;
}

