#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <grp.h>
#include <pwd.h>
#include <signal.h>
#include <syslog.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int main(){
  pid_t pid = fork();
  if(pid == 0){
    // Child process
    while(1);
  }
  else if(pid > 0){
    // Parent process
    while(1);
  }
  else{
    printf("Error : could not fork process\n");
  }
}
