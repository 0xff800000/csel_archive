#!/bin/sh

## Previous code
mount -t tmpfs none /sys/fs/cgroup
mkdir -p /sys/fs/cgroup/memory
mount -t cgroup -o memory memory /sys/fs/cgroup/memory
mkdir -p /sys/fs/cgroup/memory/mem

## New code
mkdir -p /sys/fs/cgroup/cpuset
mount -t cgroup -o cpu,cpuset cpuset /sys/fs/cgroup/cpuset
mkdir -p /sys/fs/cgroup/cpuset/high
mkdir -p /sys/fs/cgroup/cpuset/low
chmod 666 /sys/fs/cgroup/cpuset/high/cpuset.cpus
echo 0 >> /sys/fs/cgroup/cpuset/high/cpuset.cpus
echo 0 >> /sys/fs/cgroup/cpuset/high/cpuset.mems
echo 3 >> /sys/fs/cgroup/cpuset/low/cpuset.cpus
echo 0 >> /sys/fs/cgroup/cpuset/low/cpuset.mems
