#!/bin/sh

echo 3 >> /sys/fs/cgroup/cpuset/high/cpuset.cpus
echo 0 >> /sys/fs/cgroup/cpuset/high/cpuset.mems
echo 75 >> /sys/fs/cgroup/cpuset/high/cpu.shares
echo 3 >> /sys/fs/cgroup/cpuset/low/cpuset.cpus
echo 0 >> /sys/fs/cgroup/cpuset/low/cpuset.mems
echo 25 >> /sys/fs/cgroup/cpuset/low/cpu.shares
