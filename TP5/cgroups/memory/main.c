#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <grp.h>
#include <pwd.h>
#include <signal.h>
#include <syslog.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int main(){
  const int nb_blocks = 50;
  const int _1MB = (1<<20);
  int total = 0;

  for(int n=0; n<nb_blocks; n++){
    char* blocks = (char*) malloc(_1MB);
    if(blocks == 0){
      printf("Error : could not allocate memory\n");
      exit(-1);
    }
    total += _1MB;
    printf("Total of bytes %d\n", total);
    printf("Blocks pointer %p\n", blocks);

    for(int i=0; i<_1MB; i++){
      *(blocks+i) = 0;
    }
  }

  
  while(1){
    sleep(1000);
  }
}
