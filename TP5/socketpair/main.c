
/*
 * TP5 CSEL
 * Exercice 1
 * Processus, signaux et communication 
 * Conus Vincent
 * 23.11.2018
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// for signals
#include <signal.h>

// includes for processes 
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

// include socket
#include <sys/socket.h>

/* Messages from child process to parent */
#define MSG_SIZE 100
#define NM_MSG 4

static void catch_signal (int signal)
{
  printf("Signal %d catched\n", signal);
}

void child (int socket_fd, pid_t ppid)
{
  char messages[NM_MSG][MSG_SIZE] =
    {
     "** Four messages to the parent process **\n",
     "** from the child process **\n",
     "** every 2 seconds **\n",
     "exit"
    };

  for(int i = 0; i<=NM_MSG; i++)
    {
       write(socket_fd, messages[i], MSG_SIZE);       
       int err = kill(ppid, SIGUSR1); // send signal to wake the parent
       if (err == -1) perror("Error with kill function in child process");
       sleep(2);
    }
  exit(0);
}

void parent (int socket_fd)
{
  for(;;)
    {
      char buffer[MSG_SIZE];
      read(socket_fd, buffer, MSG_SIZE);

      if (strcmp(buffer, "exit") == 0) /* if message = exit */
	{
	  printf("Final message received: %s\n", buffer);
	  return;
	}
      else
	{
	  printf("%s", buffer); 
	}
      
      pause(); /* wait here until a signal from child is received */
    }
}

int main()
{

  /* catch all signals */
  struct sigaction act = {.sa_handler = catch_signal,};
  for (int si=0; si<=SIGRTMAX; si++)
    {
      int err = sigaction (si, &act, NULL);
      if(err == -1)
	{
	 fprintf(stderr, "Error while catching signal %d : ", si);
	 perror("");
       }
    }

  /* prepare for the socket */
  int fd[2];
  int err = socketpair(AF_UNIX, SOCK_DGRAM, 0, fd);
  if(err == -1) perror("Error with socket setup");
  

  /* fork in a child and a parent process */
  pid_t ppid = getpid();
  pid_t pid = fork();
  
  if (pid == 0) /* child pid*/
    {
      child(fd[1], ppid);
    }
  else if (pid > 0) /* parent pid */
    {
      parent(fd[0]);
    }
  else perror("Fork error");

  return 0;
}
