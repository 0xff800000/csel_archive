#include "HostCounter.h"

#include <algorithm> // for std::find
#include <iostream>


HostCounter::HostCounter()
{
    std::hash<std::string> hash_fn();
}

bool HostCounter::isNewHost(int hostname)
{
    return myHosts.find(hostname) == myHosts.end();
}

void HostCounter::notifyHost(std::string hostname)
{
    int hash = hash_fn(hostname);
    // add the host in the list if not already in
    if(isNewHost(hash))
    {
        myHosts.insert(hash);
    }
}

int HostCounter::getNbOfHosts()
{
    return myHosts.size();
}
