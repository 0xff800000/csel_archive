
/*
 * Mesure de la latence de la mesure du temps
 *
 */


#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#define ITER 100

static long long array[ITER];


int main (void)
{
  struct timespec t1, t2;
  
  for (int i=0; i<ITER; i++)
    {

    clock_gettime(CLOCK_MONOTONIC, &t1);

    // empty latency
  
    clock_gettime(CLOCK_MONOTONIC, &t2);

    long long overhead = (long long)(t2.tv_sec - t1.tv_sec) * 1000000000 + t2.tv_nsec -t1.tv_nsec;

    printf ("%lld ns ", overhead);
    array[i] = overhead;
  
    }

  
  return 0;
}

