/**
 * Copyright 2015 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems Laboratory
 *
 * Abstract: 	Process and daemon samples
 *
 * Purpose:	This module implements a simple daemon to be launched
 *		from a /etc/init.d/S??_* script
 *		--> this application requires /opt/daemon as root directory
 *
 * Autĥor:	Daniel Gachet
 * Date:	17.11.2015
 */

#include "daemon.h"

static int signal_catched = 0;

// functions declared in main()
extern void increase_fan_speed();
extern void decrease_fan_speed();
extern void automatic_fan_speed();
extern void manual_fan_speed();


static void catch_signal (int signal)
{
	syslog (LOG_INFO, "signal=%d catched\n", signal);
	signal_catched++;
	switch(signal){
		case SIGALRM:
			// Switch manual mode
			manual_fan_speed();
			break;
		case SIGVTALRM:
			// Switch automatic mode
			automatic_fan_speed();
			break;
		case SIGUSR1:
			// Increase speed
			increase_fan_speed();
			break;
		case SIGUSR2:
			// Decrease speed
			decrease_fan_speed();
			break;
		default:
			break;
	}
}

static void fork_process()
{
	pid_t pid = fork();
	switch (pid) {
	case  0: break; // child process has been created
	case -1: syslog (LOG_ERR, "ERROR while forking"); exit (1); break;
	default: exit(0);  // exit parent process with success
	}
}

void init_daemon()
{
	// 1. fork off the parent process
	fork_process();

	// 2. create new session
	if (setsid() == -1) {
		syslog (LOG_ERR, "ERROR while creating new session");
		exit (1);
	}

	// 3. fork again to get rid of session leading process
	fork_process();

	// 4. capture all required signals
	struct sigaction act = {.sa_handler = catch_signal,};
	sigaction (SIGHUP,  &act, NULL);  //  1 - hangup
	sigaction (SIGINT,  &act, NULL);  //  2 - terminal interrupt
	sigaction (SIGQUIT, &act, NULL);  //  3 - terminal quit
	sigaction (SIGABRT, &act, NULL);  //  6 - abort
	sigaction (SIGTERM, &act, NULL);  // 15 - termination
	sigaction (SIGTSTP, &act, NULL);  // 19 - terminal stop signal
	if(signal (SIGALRM, catch_signal) == SIG_ERR) syslog(LOG_ERR, "ERROR : signal");
	if(signal (SIGVTALRM, catch_signal) == SIG_ERR) syslog(LOG_ERR, "ERROR : signal");
	if(signal (SIGUSR1, catch_signal) == SIG_ERR) syslog(LOG_ERR, "ERROR : signal");
	if(signal (SIGUSR2, catch_signal) == SIG_ERR) syslog(LOG_ERR, "ERROR : signal");

	// 5. update file mode creation mask
	umask(0027);

	// 6. change working directory to appropriate place
	if (chdir ("/") == -1) {
		syslog (LOG_ERR, "ERROR while changing to working directory");
		exit (1);
	}

	// 7. close all open file descriptors
	for (int fd = sysconf(_SC_OPEN_MAX); fd >= 0; fd--) {
		close (fd);
	}

	// 8. redirect stdin, stdout and stderr to /dev/null
	if (open ("/dev/null", O_RDWR) != STDIN_FILENO) {
		syslog (LOG_ERR, "ERROR while opening '/dev/null' for stdin");
		exit (1);
	}
	if (dup2 (STDIN_FILENO, STDOUT_FILENO) != STDOUT_FILENO) {
		syslog (LOG_ERR, "ERROR while opening '/dev/null' for stdout");
		exit (1);
	}
	if (dup2 (STDIN_FILENO, STDERR_FILENO) != STDERR_FILENO) {
		syslog (LOG_ERR, "ERROR while opening '/dev/null' for stderr");
		exit (1);
	}

	// 9. option: open syslog for message logging
	openlog (NULL, LOG_NDELAY | LOG_PID, LOG_DAEMON);
	syslog (LOG_INFO, "Daemon has started...");

	// 10. option: get effective user and group id for appropriate's one
	struct passwd* pwd = getpwnam ("daemon");
	if (pwd == 0) {
		syslog (LOG_ERR, "ERROR while reading daemon password file entry");
		exit (1);
	}

	// 11. option: change root directory
	if (chroot (".") == -1) {
		syslog (LOG_ERR, "ERROR while changing to new root directory");
		exit (1);
	}

	// 12. option: change effective user and group id for appropriate's one
	if (setegid (pwd->pw_gid) == -1) {
		syslog (LOG_ERR, "ERROR while setting new effective group id");
		exit (1);
	}
	if (seteuid (0) == -1) { // Give root for sysfs access
	// if (seteuid (pwd->pw_uid) == -1) {
		syslog (LOG_ERR, "ERROR while setting new effective user id");
		exit (1);
	}
}

void deinit_daemon(){
	syslog (LOG_INFO, "daemon stopped. Number of signals catched=%d\n", signal_catched);
	closelog();
}
