/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Project:	HEIA-FR / HES-SO MSE - MA-CSEL1 Laboratory
 *
 * Abstract: System programming -  file system
 *
 * Purpose:	NanoPi silly status led control system
 *
 * Autĥor:	Daniel Gachet
 * Date:	07.11.2018
 *
 *
 * Modifs: Baumgartner Dan Yvan
 *
 *
 */
#include <sys/types.h>
#include <sys/stat.h>

/* epoll and timerfd */
#include <sys/epoll.h>
#include <sys/timerfd.h>
#include <sys/mman.h>

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "daemon.h"
#include "ssd1306.h"



/*
 * K1 switch  ---> gpio0
 * K2 switch  ---> gpio2
 * K3 switch  ---> gpio3
 * status led - gpioa.10 --> gpio10
 * power led  - gpiol.10 --> gpio362
 */
#define GPIO_EXPORT	"/sys/class/gpio/export"
#define GPIO_UNEXPORT	"/sys/class/gpio/unexport"
#define GPIO_LED	"/sys/class/gpio/gpio10"
#define LED	  "10"
#define GPIO_LED_POWER	"/sys/class/gpio/gpio362"
#define LED_POWER	  "362"

#define GPIO_K1		"/sys/class/gpio/gpio0"
#define K1				"0"
#define GPIO_K2		"/sys/class/gpio/gpio2"
#define K2				"2"
#define GPIO_K3		"/sys/class/gpio/gpio3"
#define K3				"3"

#define TEMP_PATH "/sys/class/thermal/thermal_zone0/temp"

#define SYSFS_FREQ "/sys/devices/platform/skeleton/freq"
#define SYSFS_MODE "/sys/devices/platform/skeleton/mode"

#define SYS_BUFF_SIZE 256

#define MAX_EVENTS 4

#define OLED_W 16

int led;
int tfd;
struct itimerspec timerValue;
uint32_t* map_temp;

void increase_fan_speed();
void decrease_fan_speed();
void toggle_fan_speed();

struct gpio{
	char* gpio_path;
	char* gpio_pin;
	int fd;
};

struct gpio power_led = {
    .gpio_path = GPIO_LED_POWER,
    .gpio_pin = LED_POWER,
    .fd = 0
};

static void open_led_struct(struct gpio * pin){
    char path[256];
	// unexport pin out of sysfs (reinitialization)
	int f = open (GPIO_UNEXPORT, O_WRONLY);
    if(f < 0){
        syslog(LOG_ERR," ---> Unexport failed");
        exit(-1);
    }
	write (f, pin->gpio_pin, strlen(pin->gpio_pin));
	close (f);

	// export pin to sysfs
	f = open (GPIO_EXPORT, O_WRONLY);
	write (f, pin->gpio_pin, strlen(pin->gpio_pin));
    if(f < 0){
        syslog(LOG_ERR," ---> Export failed");
        exit(-1);
    }
	close (f);	

	// config pin
    strcpy(path, (const char*)pin->gpio_path);
    strcat(path, "/direction");
	f = open (path, O_WRONLY);
	write (f, "out", 3);
    if(f < 0){
        syslog(LOG_ERR," ---> Set direction failed");
        exit(-1);
    }
	close (f);

	// open gpio value attribute
    strcpy(path, (const char*)pin->gpio_path);
    strcat(path, "/value");
    printf(path);
 	f = open (path, O_RDWR);
    if(f < 0){
        syslog(LOG_ERR," ---> Open value failed");
        exit(-1);
    }
    pin->fd = f;
}

/* One struct to rule 'em all */
enum{eIncrease, eDecrease, eReset};
struct event_ctl{
  int fd;
  void (*process)(struct event_ctl *event);
  int function;
};

void set_time(){
	if(
		timerfd_settime(
			tfd,
			TFD_TIMER_ABSTIME,
			&timerValue,
			NULL
		)
	){
		syslog(LOG_ERR," ---> timerfd_settime error");
		exit(-1);
	}
	syslog(LOG_INFO, "Period changed to %d second(s)", timerValue.it_interval.tv_sec);
}

void reset_timer_freq(){
	syslog(LOG_INFO,"%s\n", __FUNCTION__);
	timerValue.it_value.tv_sec = 1;
	timerValue.it_value.tv_nsec = 0;
	timerValue.it_interval.tv_sec = 1;
	timerValue.it_interval.tv_nsec = 0;
	set_time();
}

void oled_printf(char *str){
    int rem = 16 - strlen(str);
    ssd1306_puts(str);
    if(rem > 0) for(int i=0;i<rem;i++) ssd1306_putc(' ');
}

void process_timer(struct event_ctl *event){
	uint64_t buf;
	read(event->fd, &buf, sizeof(uint64_t));
	// syslog(LOG_INFO,"process_timer() : event on fd = %d after %d sec\n",
	// 	event->fd,
	// 	timerValue.it_interval.tv_sec
	// );

    // Update oled screen
    char str_mode[OLED_W] = {0};
    char str_freq[OLED_W] = {0};
    char str_temp[OLED_W] = {0};
    int fd = open(SYSFS_MODE, O_RDONLY);
    if(fd < 0){ syslog(LOG_ERR,"Error : could not open sysfs"); exit(-1);}
    read(fd, str_mode, OLED_W<<2);
    close(fd);

    fd = open(SYSFS_FREQ, O_RDONLY);
    if(fd < 0){ syslog(LOG_ERR,"Error : could not open sysfs"); exit(-1);}
    read(fd, str_freq, OLED_W);
    close(fd);

    fd = open(TEMP_PATH, O_RDONLY);
    if(fd < 0){ syslog(LOG_ERR,"Error : could not open sysfs 1"); exit(-1);}
    read(fd, str_temp, OLED_W);
    close(fd);

    char display[OLED_W];
    ssd1306_set_position(0, 0);
    sprintf(display, "Mode : %s", str_mode);
    oled_printf(display);
    ssd1306_set_position(0, 1);
    sprintf(display, "Speed : %s", str_freq);
    oled_printf(display);
    ssd1306_set_position(0, 2);
    sprintf(display, "Temp : %s", str_temp);
    oled_printf(display);
}

void process_button(struct event_ctl *event){
	char buf;
	lseek(event->fd, 0, SEEK_SET);
	read(event->fd, &buf, 1);
	// syslog(LOG_INFO,"process_button() : event on fd = %d value=%c\n",
	// 	event->fd,
	// 	buf
	// );
    pwrite(power_led.fd, (void *)&buf, 1, 0);
	if(buf == '1'){
		switch(event->function){
			case eIncrease:
				increase_fan_speed();
				break;
			case eDecrease:
				decrease_fan_speed();
				break;
			case eReset:
				toggle_fan_speed();
				break;
		}
	}
}


struct button{
	char* gpio_path;
	char* gpio_pin;
	int fd;
	int function;
};


int configure_button(struct button* sw, int len){
	int ret = -1;
	for(int i=0; i<len; i++){
		char path[256];
		// unexport pin out of sysfs (reinitialization)
		int f = open (GPIO_UNEXPORT, O_WRONLY);
		write (f, sw[i].gpio_pin, strlen(sw[i].gpio_pin));
		if(f < 0){
			syslog(LOG_ERR," ---> Unexport failed");
			exit(-1);
		}
		close (f);

		// export pin to sysfs
		f = open (GPIO_EXPORT, O_WRONLY);
		if(f < 0){
			syslog(LOG_ERR," ---> Export failed");
			exit(-1);
		}
		write (f, sw[i].gpio_pin, strlen(sw[i].gpio_pin));
		close (f);

		// config pin
		strcpy(path, (const char*)sw[i].gpio_path);
		strcat(path, "/direction");
		f = open (path, O_WRONLY);
		if(f < 0){
			syslog(LOG_ERR," ---> Set direction failed");
			exit(-1);
		}
		write (f, "in", 2);
		close (f);
		strcpy(path, (const char*)sw[i].gpio_path);
		strcat(path, "/edge");
		f = open (path, O_WRONLY);
		if(f < 0){
			syslog(LOG_ERR," ---> Set edge failed");
			exit(-1);
		}
		write (f, "both", 4);
		close (f);

		// open gpio value attribute
		strcpy(path, (const char*)sw[i].gpio_path);
		strcat(path, "/value");
 		f = open (path, O_RDWR);
		if(f < 0){
			syslog(LOG_ERR," ---> Open value failed");
			exit(-1);
		}
		sw[i].fd = f;
		if(f > ret) ret = f;
	}
	return ret;
}

// Helper functions
void increase_fan_speed(){
    syslog(LOG_INFO,"Fan speed increased\n");
    int fd = open(SYSFS_FREQ, O_RDWR);
    if(fd < 0){
        syslog(LOG_INFO,"Error : could not open sysfs\n");
        exit(-1);
    }

    char buff[SYS_BUFF_SIZE];
    read(fd, buff, SYS_BUFF_SIZE);
    // syslog(LOG_INFO,"%s",buff);
    int frequency;
    sscanf(buff, "%d",&frequency);
    syslog(LOG_INFO,"current frequency %d\n", frequency);
    frequency+=100;
    sprintf(buff, "%d\n", frequency);
    write(fd, buff, strlen(buff));
    close(fd);
    return;
}

void decrease_fan_speed(){
    syslog(LOG_INFO,"Fan speed decreased\n");
    int fd = open(SYSFS_FREQ, O_RDWR);
    if(fd < 0){
        syslog(LOG_INFO,"Error : could not open sysfs\n");
        exit(-1);
    }

    char buff[SYS_BUFF_SIZE];
    read(fd, buff, SYS_BUFF_SIZE);
    // syslog(LOG_INFO,"%s",buff);
    int frequency;
    sscanf(buff, "%d",&frequency);
    syslog(LOG_INFO,"current frequency %d\n", frequency);
    frequency-=100;
    sprintf(buff, "%d\n", frequency);
    if(frequency > 0) write(fd, buff, strlen(buff));
    close(fd);
    return;
}

void toggle_fan_speed(){
    syslog(LOG_INFO,"Fan speed toggled\n");
    int fd = open(SYSFS_MODE, O_RDWR);
    if(fd < 0){
        syslog(LOG_INFO,"Error : could not open sysfs\n");
        exit(-1);
    }

    char buff[SYS_BUFF_SIZE];
    read(fd, buff, SYS_BUFF_SIZE);
    syslog(LOG_INFO,"Current mode %s",buff);
    if(NULL != strstr(buff,"auto")){
        strcpy(buff, "manual\n");
    }
    else{
        strcpy(buff, "auto\n");
    }
    syslog(LOG_INFO,"Mode changed to : %s\n", buff);
    write(fd, buff, strlen(buff));
    close(fd);
    return;
}

void automatic_fan_speed(){
    syslog(LOG_INFO,"Fan speed automatic\n");
    int fd = open(SYSFS_MODE, O_RDWR);
    if(fd < 0){
        syslog(LOG_INFO,"Error : could not open sysfs\n");
        exit(-1);
    }

    write(fd, "auto\n", strlen("auto\n"));
    close(fd);
    return;
}

void manual_fan_speed(){
    syslog(LOG_INFO,"Fan speed manual\n");
    int fd = open(SYSFS_MODE, O_RDWR);
    if(fd < 0){
        syslog(LOG_INFO,"Error : could not open sysfs\n");
        exit(-1);
    }

    write(fd, "manual\n", strlen("manual\n"));
    close(fd);
    return;
}

int main(int argc, char* argv[]){
    // Init daemon
    init_daemon();

    // Init oled
    ssd1306_init();
    ssd1306_puts("hello world");

	/* events structures */
	struct epoll_event event_timer;
	struct event_ctl events_ctl[MAX_EVENTS];

	/* Create time file descriptor */
	tfd = timerfd_create(CLOCK_REALTIME, 0);
	if (tfd == -1){
		syslog(LOG_ERR," ---> Error with timerfd");
		exit(-1);
	}
	syslog(LOG_INFO,"Timer fd=%d\n",tfd);
	events_ctl[0].fd = tfd;
	events_ctl[0].process = process_timer;

	bzero(&timerValue, sizeof(timerValue));
	reset_timer_freq();

	/* Création du contexte epoll*/
	int epfd = epoll_create1(0);
	if (epfd == -1){
		syslog(LOG_ERR," ---> Error when starting epoll");
		exit(-1);
	}

	event_timer.events = EPOLLIN | EPOLLET; // only for input and rising edge
	event_timer.data.fd = tfd;
	event_timer.data.ptr = &events_ctl[0];

	/* add the timerfd to epoll */
	if (epoll_ctl(epfd, EPOLL_CTL_ADD, tfd, &event_timer)){
		syslog(LOG_ERR," ---> epoll ctl error");
		exit(-1);
	}

	/* set timer */
	set_time();

	/* LEDs setup */
    open_led_struct(&power_led);

	/* Buttons */
	/* Init number buttons */
	struct button buttons[] = {
		[0] = {
			.gpio_path = GPIO_K1,
			.gpio_pin = K1,
			.fd = 0,
			.function = eIncrease,
		},
		[1] = {
			.gpio_path = GPIO_K2,
			.gpio_pin = K2,
			.fd = 0,
			.function = eDecrease,
		},
		[2] = {
			.gpio_path = GPIO_K3,
			.gpio_pin = K3,
			.fd = 0,
			.function = eReset,
		}
	};

	configure_button(buttons, sizeof(buttons)/sizeof(struct button));

	struct epoll_event events[MAX_EVENTS] = {0};
	for(int i=0; i<sizeof(buttons)/sizeof(struct button); i++){
		events[i].events = EPOLLET;
		events[i].data.fd = buttons[i].fd;
		events[i].data.ptr = &events_ctl[i+1];

		events_ctl[i+1].fd = buttons[i].fd;
		events_ctl[i+1].process = process_button;
		events_ctl[i+1].function = buttons[i].function;

		/* add the buttons to the epoll (fd and event list)*/
		int ret = epoll_ctl(epfd, EPOLL_CTL_ADD, buttons[i].fd, &events[i]);

		syslog(LOG_INFO,"Register %d\n", buttons[i].fd);

		if(ret < 0){
			syslog(LOG_ERR,"Epoll init ctl");
			exit(-1);
		}
	}


	/***********************************/
	// final loop
	while(1){
		int nr = epoll_wait (epfd, events, MAX_EVENTS, -1);
		if(nr == -1){
			syslog(LOG_ERR," ---> epoll error");
			// exit(-1);
		}
		else if (nr == 0){syslog(LOG_ERR," ---> nothing received!");}
		else {
			for (int i=0; i<nr; i++){
				struct event_ctl* event = events[i].data.ptr;
				if(event->process != NULL) event->process(event);
			}
		}
	}

    deinit_daemon();

	return 0;

}

