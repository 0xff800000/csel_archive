/* main file to test sysfs char driver */
#include <linux/module.h> /* modules */
#include <linux/init.h> /* macros */
#include <linux/kernel.h> /* debugging */
#include <linux/string.h> /* handles strings */
#include <linux/list.h> /* listes */
#include <linux/slab.h>
#include <linux/gfp.h>
#include <linux/moduleparam.h> /* kernel parameters */
#include <linux/io.h> /* entrées/sorties */
#include <linux/uaccess.h> /* copie entre noyau et userspace */
#include <linux/fs.h> /* operation du pilote */
#include <linux/cdev.h>
#include <linux/device.h>


dev_t dev = 0;
static struct class *dev_class;
static struct cdev driver_cdev;


static int __init sysfs_driver_init(void);
static void __exit sysfs_driver_exit(void);
 

static int driver_open (struct inode *i,
			struct file *f);
static int driver_release (struct inode *i,
			   struct file *f);
static ssize_t driver_read( struct file *f,
			    char __user *buf,
			    size_t count,
			    loff_t *off);
static ssize_t driver_write( struct file *f,
			     char __user *buf,
			     size_t count,
			     loff_t *off);

static struct file_operations fops =
  {
   .owner = THIS_MODULE,
   .open = driver_open,
   .read = driver_read,
   .write = driver_write,
   .release = driver_release,
  };

// exemple open function
static int driver_open (struct inode *i, struct file *f)
{
  
}

// exemple read function
static ssize_t
driver_read( struct file *f,
	       char __user *buf,
	       size_t count,
	       loff_t *off)
{
  
}

// exemple write function
static ssize_t
driver_write( struct file *f,
	       char __user *buf,
	       size_t count,
	       loff_t *off)
{
  
}


static int driver_release (struct inode *i, struct file *f)
{
  pr_info ("sysfs driver: release operation...\n");

  return 0;
}


static int __init sysfs_driver_init(void)
{

  if( alloc_chrdev_region (&dev,
			   0,
			   1,
			   "sysfsDriver") <0)
    {
      pr_info("** Cannot allocate major number\n");
      return -1;
    }
  pr_info ("** Linux driver device skeleton loaded **\n");
  pr_info ("Major = %d Minor = %d \n",MAJOR(dev), MINOR(dev));


  /*Creating device*/
  if((device_create(dev_class,NULL,dev,NULL,"etx_device")) == NULL){
    pr_info("** Cannot create the Device 1\n");
    return -1;
  }


  /* init the cdev */
  cdev_init (&driver_cdev, &fops);

  /*  add character device */
  if((cdev_add (&driver_cdev, dev, 1))<0)
    {
      pr_info("** Cannot add the device to the system\n");
    }

  /*Creating struct class*/
  if((dev_class = class_create(THIS_MODULE,"sysfs_class")) == NULL){
    pr_info("** Cannot create the struct class\n");
    return -1;
  }

  return 0;
}


static void __exit sysfs_driver_exit(void)
{



  
  return 0;
}


module_init (driver_init);
module_exit (driver_exit);

MODULE_AUTHOR ("Vincent Conus <vincent.conus@master.hes-so.ch>");
MODULE_DESCRIPTION ("Driver sysfs char oriented for mini project");
MODULE_LICENSE ("GPL");

