/* skeleton.c for char pilote */
#include <linux/module.h> /* modules */
#include <linux/init.h> /* macros */
#include <linux/kernel.h> /* debugging */
#include <linux/string.h> /* handles strings */
#include <linux/list.h> /* listes */
#include <linux/slab.h>
#include <linux/gfp.h>
#include <linux/moduleparam.h> /* kernel parameters */
#include <linux/io.h> /* entrées/sorties */
#include <linux/uaccess.h> /* copie entre noyau et userspace */
#include <linux/fs.h> /* operation du pilote */
#include <linux/cdev.h>

/* specific to sysfs*/
#include <linux/device.h>
#include <linux/platform_device.h>

/* LEDs  */
#include <linux/gpio.h>
#define GPIO_LED    10
static bool ledOn = 0;//LED on or off? Used to invert its state

/* CPU temperature */
#include <linux/thermal.h>

/* timer */
#include <linux/timer.h>
static struct timer_list my_timer;
int timer_interval = 1000;

struct buf
{
  char mode_buf[1000];
  char freq_buf[1000];
};
struct buf sysfs_buf;



int gpio_init(void)
{
  /* LED */
  if (!gpio_is_valid(GPIO_LED)){
    pr_info ("** GPIO_TEST: invalid LED GPIO\n");
    return -1;
  }
   // Going to set up the LED.
   // It is a GPIO in output mode and will be on by default
   ledOn = true;
   gpio_request(GPIO_LED, "sysfs");
   gpio_direction_output(GPIO_LED, ledOn);

   return 0;
}

void gpioLED_change(void)
{
  ledOn = !ledOn;
  gpio_set_value(GPIO_LED, ledOn);
}

void automode_checker(void)
{
  int temp;
  
  thermal_zone_get_temp(thermal_zone_get_zone_by_name("cpu_thermal"), &temp);
  //pr_info ("**Current temp of CPU: %d **\n", temp);

  if (temp < 35000)
    {
      timer_interval = 500; /* 2 Hz */
    }
  else if (temp < 40000)
    {
      timer_interval = 200; /* 5 Hz */
    }
  else if (temp < 45000)
    {
      timer_interval = 100; /* 10 Hz */
    }
  else /* above 45000 */
    {
      timer_interval = 50; /* 20 Hz */
    }

  // put the time value in the sysfs buffer
  //pr_info ("Timer interval is now %d", timer_interval);
  sprintf(sysfs_buf.freq_buf, "%d", timer_interval);

}


void my_timer_callback(struct timer_list  *timer)
{
 
  if (strncmp(sysfs_buf.mode_buf, "auto", 4) == 0)
    {
      //pr_info ("Auto mode");
      automode_checker();
    }
  else if (strncmp(sysfs_buf.mode_buf, "manual", 6) == 0)
    {
      //pr_info ("Manuel mode");
      sscanf (sysfs_buf.freq_buf, "%d", &timer_interval);
      //pr_info ("%d", timer_interval);
    }
  else
    {
      // EMPTY
    }
  
  
  gpioLED_change();

  /* reset timer ?? */
  mod_timer(&my_timer, jiffies + msecs_to_jiffies(timer_interval));

  //pr_info("** Timer callback function\n");
}


static ssize_t skeleton_show_mode ( struct device* dev,
				   struct device_attribute *attr,
				   char *buf)
{
  strcpy (buf, sysfs_buf.mode_buf);
  // pr_info("sysfs : sysfs_buf read\n");
  return strlen (sysfs_buf.mode_buf);
}

static ssize_t skeleton_store_mode ( struct device* dev,
				    struct device_attribute *attr,
				    const char *buf,
				    size_t count)
{
  int len = sizeof(sysfs_buf.mode_buf) -1;
  if (len > count) len = count;
  strncpy (sysfs_buf.mode_buf, buf, len);
  sysfs_buf.mode_buf[len] = 0;
    // pr_info("sysfs : sysfs_buf written\n");
  return len;
}


static ssize_t skeleton_show_freq ( struct device* dev,
				   struct device_attribute *attr,
				   char *buf)
{
  strcpy (buf, sysfs_buf.freq_buf);
  // pr_info("sysfs : sysfs_buf read\n");
  return strlen (sysfs_buf.freq_buf);
}

static ssize_t skeleton_store_freq ( struct device* dev,
				    struct device_attribute *attr,
				    const char *buf,
				    size_t count)
{
  int len = sizeof(sysfs_buf.freq_buf) -1;
  if (len > count) len = count;
  strncpy (sysfs_buf.freq_buf, buf, len);
  sysfs_buf.freq_buf[len] = 0;
    // pr_info("sysfs : sysfs_buf written\n");
  return len;
}



/* sysfs handeling methodes */

static void sysfs_dev_release (struct device *dev)
{
  // empty 
}

static struct platform_driver sysfs_driver =
  {
   .driver =  {.name = "skeleton",},
  };

static struct platform_device sysfs_device =
  {
   .name = "skeleton",
   .id = -1,
   .dev.release = sysfs_dev_release,
  };


/* #### Define the device attributes ####*/
DEVICE_ATTR (mode, 0664, skeleton_show_mode, skeleton_store_mode);
DEVICE_ATTR (freq, 0664, skeleton_show_freq, skeleton_store_freq);

/* Module init function */
static int __init skeleton_init(void)
{
  int status = 0;

  // default mode: automatic
  strcpy (sysfs_buf.mode_buf, "auto");

  
  /* sysfs */ 
  if (status == 0){
    platform_driver_register (&sysfs_driver);}
  if (status == 0){
    platform_device_register (&sysfs_device);}
  if (status == 0){
    device_create_file (&sysfs_device.dev, &dev_attr_mode);
    device_create_file (&sysfs_device.dev, &dev_attr_freq);}
  pr_info ("** Linux driver sysfs skeleton loaded **\n");

  
  /* LEDs */
  gpio_init();
  pr_info ("** GPIO for LEDs control started **\n");

  
  /* timer */
  timer_setup(&my_timer, my_timer_callback, 0);
  mod_timer(&my_timer, jiffies + msecs_to_jiffies(timer_interval));
  pr_info ("** Timer and callback started **\n");
  
  return status;
}

/* Module exit function */
static void __exit skeleton_exit(void)
{
    
  /* sysfs */
  device_remove_file(&sysfs_device.dev, &dev_attr_mode);
  device_remove_file(&sysfs_device.dev, &dev_attr_freq);
  platform_device_unregister (&sysfs_device);
  platform_driver_unregister (&sysfs_driver);
  pr_info ("** Linux driver sysfs skeleton unloaded **\n");

  
  /* LEDs */
  gpio_free(GPIO_LED);
  pr_info ("** GPIO for LEDs control freed **\n");

  
  /* timer */
  del_timer(&my_timer);
  pr_info ("** Timer and callback stoped **\n");
  
}


module_init (skeleton_init);
module_exit (skeleton_exit);

MODULE_AUTHOR ("Vincent Conus <vincent.conus@master.hes-so.ch>");
MODULE_DESCRIPTION ("Driver module skeleton");
MODULE_LICENSE ("GPL");
