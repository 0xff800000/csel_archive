#include "ssd1306.h"

int main()
{
    ssd1306_init();

    ssd1306_set_position (0,0);
    ssd1306_puts("CSEL1a - SP.07");
    ssd1306_set_position (0,1);
    ssd1306_puts("Temp: 35'C");
    ssd1306_set_position (0,2);
    ssd1306_puts("Freq: 1Hz");
    ssd1306_set_position (0,3);
    ssd1306_puts("Duty: 50%");

    return 0;
}

