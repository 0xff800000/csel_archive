#!/bin/sh
# ------------------------------------------------------------------
# [Vincent Conus] CSEL Miniproject
#                 Control LED simulating a CPU fan cooler
# ------------------------------------------------------------------

__VERSION=1.0
__SUBJECT=fanLED
__USAGE="
Usage: fanLED [OPTIONS]

Options:
	-m       :   Switch to manual mode
	-a       :   Switch to automatic mode
	-t       :   Displays CPU temperature

	-v       :   Version
	-h       :   Shows this help
	-k       :   Kill the daemon

Options for manual mode:
	-i  <n>  :   Increase the LED blinking rate <n> times
	-d  <n>  :   Decrease the LED blinking rate <n> times
"


# --- Get the deamon PID (test with random daemon)
__PID=$(pidof fanctrl)
if [ -z "$__PID" ]
then
    echo "ERROR : daemon not launched"
    exit 1;
fi


# --- Options processing -------------------------------------------
if [ $# == 0 ] ; then
    echo "$__USAGE"
    exit 1;
fi

while getopts ":i:d:matvhk" optname
do
    case "$optname" in
	"m")
	    kill -s SIGALRM $__PID
            echo "Manual mode enabled"
            exit 0;
            ;;
	"a")
	    kill -s SIGVTALRM $__PID
            echo "Automatic mode enabled"
            exit 0;
            ;;
	"t")
            echo "Current CPU temperature is" $(cat /sys/class/thermal/thermal_zone0/temp)
            exit 0;
            ;;
	"v")
            echo "Version $__VERSION"
            exit 0;
            ;;
	"h")
            echo "$__USAGE"
            exit 0;
            ;;
	"i")
	    for i in $(seq "$OPTARG")
	    do
		kill -s SIGUSR1 $__PID
        sleep 0
	    done
            echo "-i argument: $OPTARG"
            ;;
	"d")
        for i in $(seq "$OPTARG")
	    do
	    kill -s SIGUSR2 $__PID
        sleep 0
	    done
            echo "-d argument: $OPTARG"
            ;;
	"k")
	    kill -9 $__PID
            echo "Daemon killed"
            ;;

	"?")
            echo "Unknown option $OPTARG"
            exit 0;
            ;;
	*)
            echo "Unknown error while processing options"
            exit 0;
            ;;
    esac
done

shift $(($OPTIND - 1))

# --- Locks -------------------------------------------------------
__LOCK_FILE=/tmp/$__SUBJECT.lock
if [ -f "$__LOCK_FILE" ]; then
   echo "Miniproject UI script is already running"
   exit
fi

trap "rm -f $__LOCK_FILE" EXIT
touch $__LOCK_FILE
